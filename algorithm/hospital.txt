PART 01: REGISTRATION OF PATIENT 
    1. Patient Already Exists in the System or in the Older File
        1.1 Previous Case Not Closed 
            1.1.1 All payment Settled if not settle 
            1.1.2 All required flow completed, if not complete 
            1.1.3 Close the case if 1 & 2 are settled and continue to 1.2 
        1.2 Previous Case Closed 
            1.2.1 Previous Min Registration , go to 2.2 New Patient 
            1.2.2 On-Schedule Forward to Triage 
            1.2.3 Out-Schedule 
                1.2.3.1 Reason & Approval 
                1.2.3.2 Forward to Triage 
            

    2. Patient Does Not Exists in the System and Not in Older File 
        2.1 Trasfered-IN patient
            2.1.1 Do Min Registration
            2.1.2 Open a Case
            2.1.3 Perform Registration Payment 
            2.1.4 Forward Appropriately
        2.2 New Patient 
            2.2.1 Permenant Registration (if coming from 1.2.1 upgrade existing registration)
                2.2.1.1 Perform Registration
                2.2.1.2 Open a Case
                2.2.1.3 Pay Registration Fee
                2.2.1.4 Forward to Triage
            2.2.2 Min Registration
                2.2.2.1 Perform Min Registration 
                2.2.2.2 Open a Case
                2.2.2.3 Pay Min Registration Fee 
                2.2.2.4 Forward to Triage

[]System will automatic generate invoices, put-in invoice-table and paymentLog
[]Payment will automaticall generate receipts, put in payment table and paymentLog 

