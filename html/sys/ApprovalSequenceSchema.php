<?php
/******************************************************
**                                                   **
**        CLASSNAME : ApprovalSequenceSchema         **
**  Copyright (c) Zoomtong Company Limited           **
**  Developed by : Ndimangwa Fadhili Ngoya           **
**  Timestamp    : 2021:08:01:21:14:04               **
**  Phones       : +255 787 101 808 / 762 357 596    **
**  Email        : ndimangwa@gmail.com               **
**  Address      : P.O BOX 7436 MOSHI, TANZANIA      **
**                                                   **
**  Dedication to my dear wife Valentina             **
**                my daughters Raheli & Keziah       **
**                                                   **
*******************************************************/
class ApprovalSequenceSchema extends __data__ {
	protected $database;
	protected $conn;
	private $schemaId;
	private $schemaName;
	private $operationCode;
	private $approvedJobList;
	private $approvingJobList;
	private $validity;
	private $defaultApproveText;
	private $extraFilter;
	private $extraInformation;
	private $flags;
/*BEGIN OF CUSTOM CODES : You should Add Your Custom Codes Below this line*/

/*END OF CUSTOM CODES : You should Add Your Custom Codes Above this line*/
	public static function create($database, $id, $conn) { return new ApprovalSequenceSchema($database, $id, $conn); }
	public function __construct($database, $id, $conn)    {
		$this->setMe($database, $id, $conn);
	}
	public function setMe($database, $id, $conn)    {
		$this->database = $database;
		$this->conn = $conn;
		$whereClause = self::getId0Columnname();
		$whereClause = array($whereClause => $id);
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), array('*'), $whereClause);
		$jresult1 = SQLEngine::execute($query, $conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] !== 1) throw new Exception("Duplicate or no record found");
		$resultSet = $jArray1['rows'][0];
		if (! array_key_exists("schemaId", $resultSet)) throw new Exception("Column [schemaId] not available while pulling data");
		$this->schemaId = $resultSet["schemaId"];
		if (! array_key_exists("schemaName", $resultSet)) throw new Exception("Column [schemaName] not available while pulling data");
		$this->setSchemaName($resultSet["schemaName"]);
		if (! array_key_exists("operationCode", $resultSet)) throw new Exception("Column [operationCode] not available while pulling data");
		$this->setOperationCode($resultSet["operationCode"]);
		if (! array_key_exists("approvedJobList", $resultSet)) throw new Exception("Column [approvedJobList] not available while pulling data");
		$this->setApprovedJobList($resultSet["approvedJobList"]);
		if (! array_key_exists("approvingJobList", $resultSet)) throw new Exception("Column [approvingJobList] not available while pulling data");
		$this->setApprovingJobList($resultSet["approvingJobList"]);
		if (! array_key_exists("validity", $resultSet)) throw new Exception("Column [validity] not available while pulling data");
		$this->setValidity($resultSet["validity"]);
		if (! array_key_exists("defaultApproveText", $resultSet)) throw new Exception("Column [defaultApproveText] not available while pulling data");
		$this->setDefaultApproveText($resultSet["defaultApproveText"]);
		if (! array_key_exists("extraFilter", $resultSet)) throw new Exception("Column [extraFilter] not available while pulling data");
		$this->setExtraFilter($resultSet["extraFilter"]);
		if (! array_key_exists("extraInformation", $resultSet)) throw new Exception("Column [extraInformation] not available while pulling data");
		$this->setExtraInformation($resultSet["extraInformation"]);
		if (! array_key_exists("flags", $resultSet)) throw new Exception("Column [flags] not available while pulling data");
		$this->setFlags($resultSet["flags"]);
		$this->clearUpdateList();
		return $this;
	}
	public static function loadAllData($__conn) {
		$colArray1 = array('schemaId', 'schemaName');
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), $colArray1, null);
		$jresult1 = SQLEngine::execute($query, $__conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		$dataArray1 = array();
		foreach ($jArray1['rows'] as $resultSet)    {
			$index = sizeof($dataArray1); $dataArray1[$index] = array();
			$dataArray1[$index]['__id__'] = $resultSet['schemaId'];
			$myval = "";
			$myval .= " ".$resultSet['schemaName'];
			$dataArray1[$index]['__name__'] = trim($myval);
		}
		return $dataArray1;
	}
	public function getId() { return md5($this->schemaId); }
	public function getIdWhereClause() { return "{ \"schemaId\" : $this->schemaId }"; }
	public function getId0()  { return $this->schemaId; }
	public function getId0WhereClause()  { return "{ \"schemaId\" : $this->schemaId }"; }
	public function getSchemaId(){
		return $this->schemaId;
	}
	public function setSchemaName($schemaName){
		$maxLength = self::getMaximumLength('schemaName');
		if (! (is_null($maxLength) || ! (strlen($schemaName) > $maxLength))) throw new Exception("[ schemaName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('schemaName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $schemaName) === 1)) throw new Exception("[ schemaName ] : ".$regex['message']);
		$this->schemaName = $schemaName;
		$this->addToUpdateList("schemaName", $schemaName);
		return $this;
	}
	public function getSchemaName(){
		return $this->schemaName;
	}
	public function setOperationCode($operationCode){
		$maxLength = self::getMaximumLength('operationCode');
		if (! (is_null($maxLength) || ! (strlen($operationCode) > $maxLength))) throw new Exception("[ operationCode ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('operationCode');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $operationCode) === 1)) throw new Exception("[ operationCode ] : ".$regex['message']);
		$this->operationCode = $operationCode;
		$this->addToUpdateList("operationCode", $operationCode);
		return $this;
	}
	public function getOperationCode(){
		return $this->operationCode;
	}
	public function setApprovedJobList($jobId){
		$maxLength = self::getMaximumLength('approvedJobList');
		if (! (is_null($maxLength) || ! (strlen($jobId) > $maxLength))) throw new Exception("[ approvedJobList ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('approvedJobList');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $jobId) === 1)) throw new Exception("[ approvedJobList ] : ".$regex['message']);
		if (is_null($jobId)) return $this;
		$tempArray1 = explode(",", $jobId);
		$this->approvedJobList = array();
		foreach ($tempArray1 as $apropid)  {
			$this->approvedJobList[sizeof($this->approvedJobList)] = new JobTitle($this->database, $apropid, $this->conn);
		}
		$this->addToUpdateList("approvedJobList", $jobId);
		return $this;
	}
	public function getApprovedJobList(){
		return $this->approvedJobList;
	}
	public function setApprovingJobList($jobId){
		$maxLength = self::getMaximumLength('approvingJobList');
		if (! (is_null($maxLength) || ! (strlen($jobId) > $maxLength))) throw new Exception("[ approvingJobList ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('approvingJobList');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $jobId) === 1)) throw new Exception("[ approvingJobList ] : ".$regex['message']);
		if (is_null($jobId)) return $this;
		$tempArray1 = explode(",", $jobId);
		$this->approvingJobList = array();
		foreach ($tempArray1 as $apropid)  {
			$this->approvingJobList[sizeof($this->approvingJobList)] = new JobTitle($this->database, $apropid, $this->conn);
		}
		$this->addToUpdateList("approvingJobList", $jobId);
		return $this;
	}
	public function getApprovingJobList(){
		return $this->approvingJobList;
	}
	public function setValidity($validity){
		$maxLength = self::getMaximumLength('validity');
		if (! (is_null($maxLength) || ! (strlen($validity) > $maxLength))) throw new Exception("[ validity ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('validity');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $validity) === 1)) throw new Exception("[ validity ] : ".$regex['message']);
		$this->validity = $validity;
		$this->addToUpdateList("validity", $validity);
		return $this;
	}
	public function getValidity(){
		return $this->validity;
	}
	public function setDefaultApproveText($defaultApproveText){
		$maxLength = self::getMaximumLength('defaultApproveText');
		if (! (is_null($maxLength) || ! (strlen($defaultApproveText) > $maxLength))) throw new Exception("[ defaultApproveText ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('defaultApproveText');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $defaultApproveText) === 1)) throw new Exception("[ defaultApproveText ] : ".$regex['message']);
		$this->defaultApproveText = $defaultApproveText;
		$this->addToUpdateList("defaultApproveText", $defaultApproveText);
		return $this;
	}
	public function getDefaultApproveText(){
		return $this->defaultApproveText;
	}
	public function setExtraFilter($extraFilter){
		$maxLength = self::getMaximumLength('extraFilter');
		if (! (is_null($maxLength) || ! (strlen($extraFilter) > $maxLength))) throw new Exception("[ extraFilter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraFilter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraFilter) === 1)) throw new Exception("[ extraFilter ] : ".$regex['message']);
		$this->extraFilter = $extraFilter;
		$this->addToUpdateList("extraFilter", $extraFilter);
		return $this;
	}
	public function getExtraFilter(){
		return $this->extraFilter;
	}
	public function setExtraInformation($extraInformation){
		$maxLength = self::getMaximumLength('extraInformation');
		if (! (is_null($maxLength) || ! (strlen($extraInformation) > $maxLength))) throw new Exception("[ extraInformation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraInformation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraInformation) === 1)) throw new Exception("[ extraInformation ] : ".$regex['message']);
		$this->extraInformation = $extraInformation;
		$this->addToUpdateList("extraInformation", $extraInformation);
		return $this;
	}
	public function getExtraInformation(){
		return $this->extraInformation;
	}
	public function setFlags($flags){
		$maxLength = self::getMaximumLength('flags');
		if (! (is_null($maxLength) || ! (strlen($flags) > $maxLength))) throw new Exception("[ flags ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('flags');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $flags) === 1)) throw new Exception("[ flags ] : ".$regex['message']);
		$this->flags = $flags;
		$this->addToUpdateList("flags", $flags);
		return $this;
	}
	public function getFlags(){
		return $this->flags;
	}
	public static function getId0Columnname()   { return "schemaId"; }
	public static function getIdColumnnames() { return array("schemaId"); }
	public static function getReferenceClass($pname)    {
		$tArray1 = array('approvedJobList' => 'JobTitle', 'approvingJobList' => 'JobTitle');
		$refclass = null; if (isset($tArray1[$pname])) $refclass = $tArray1[$pname];
		return $refclass;
	}
	public static function getColumnType($pname)    {
		$tArray1 = array('schemaId' => 'integer', 'schemaName' => 'text', 'operationCode' => 'integer', 'approvedJobList' => 'list-object', 'approvingJobList' => 'list-object', 'validity' => 'integer', 'defaultApproveText' => 'text', 'extraFilter' => 'text', 'extraInformation' => 'text', 'flags' => 'integer');
		$type = null; if (isset($tArray1[$pname])) $type = $tArray1[$pname];
		return $type;
	}
	public static function getRegularExpression($colname)   {
		$tArray1 = array();
		$regexArray1 = null;
		if (isset($tArray1[$colname])) $regexArray1 = $tArray1[$colname];
		return $regexArray1;
	}
	public static function getMaximumLength($colname)    {
		$tArray1 = array();
		$tArray1['schemaName'] = 48; 
		$tArray1['approvedJobList'] = 255; 
		$tArray1['approvingJobList'] = 255; 
		$tArray1['defaultApproveText'] = 48; 
		$tArray1['extraFilter'] = 32; 
		$tArray1['extraInformation'] = 64; 
		$length = null;
		if (isset($tArray1[$colname])) $length = $tArray1[$colname];
		return $length;
	}
	public function getMyClassname()    { return self::getClassname(); }
	public function getMyTablename()    { return self::getTablename(); }
	public function getMyId0Columnname()  { return self::getId0Columnname(); }
	public static function getClassname()  { return "ApprovalSequenceSchema"; }
	public static function getTablename()  { return "_approvalSequenceSchema"; }
	public static function column2Property($colname)    {
		$tArray1 = array(
			"schemaId" => "schemaId"
			, "schemaName" => "schemaName"
			, "operationCode" => "operationCode"
			, "approvedJobList" => "approvedJobList"
			, "approvingJobList" => "approvingJobList"
			, "validity" => "validity"
			, "defaultApproveText" => "defaultApproveText"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$pname = null;
		if (isset($tArray1[$colname])) $pname = $tArray1[$colname];
		return $pname;
	}
	public static function property2Column($pname)    {
		$tArray1 = array(
			"schemaId" => "schemaId"
			, "schemaName" => "schemaName"
			, "operationCode" => "operationCode"
			, "approvedJobList" => "approvedJobList"
			, "approvingJobList" => "approvingJobList"
			, "validity" => "validity"
			, "defaultApproveText" => "defaultApproveText"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$colname = null;
		if (isset($tArray1[$pname])) $colname = $tArray1[$pname];
		return $colname;
	}
	public static function getColumnLookupTable()   {
		$tArray1 = array();
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "schemaId";		$tArray1[$tsize]['pname'] = "schemaId";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "schemaName";		$tArray1[$tsize]['pname'] = "schemaName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "operationCode";		$tArray1[$tsize]['pname'] = "operationCode";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "approvedJobList";		$tArray1[$tsize]['pname'] = "approvedJobList";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "approvingJobList";		$tArray1[$tsize]['pname'] = "approvingJobList";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "validity";		$tArray1[$tsize]['pname'] = "validity";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "defaultApproveText";		$tArray1[$tsize]['pname'] = "defaultApproveText";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraFilter";		$tArray1[$tsize]['pname'] = "extraFilter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraInformation";		$tArray1[$tsize]['pname'] = "extraInformation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "flags";		$tArray1[$tsize]['pname'] = "flags";
		return $tArray1;
	}
	public static function columnTransitiveMap($pname)  {
		$tArray1 =  array('schemaId' => 'schemaId', 'schemaName' => 'schemaName', 'operationCode' => 'operationCode', 'approvedJobList' => 'approvedJobList', 'approvingJobList' => 'approvingJobList', 'validity' => 'validity', 'defaultApproveText' => 'defaultApproveText', 'extraFilter' => 'extraFilter', 'extraInformation' => 'extraInformation', 'flags' => 'flags');
		$pmap = null; if (isset($tArray1[$pname])) $pmap = $tArray1[$pname];
		return $pmap;
	}
	public static function getSearchableColumns()    {
		/* Will return list of Searchable Properties */
		return array('schemaName', 'operationCode', 'approvedJobList', 'approvingJobList', 'validity', 'defaultApproveText', 'extraFilter', 'extraInformation', 'flags');
	}
	public static function getASearchUI($page, $listOfColumnsToDisplay, $optIndex = 0)    {
		$line = "";
		$mycolumnlist = json_encode($listOfColumnsToDisplay);
		$line .= "&lt;div class=&quot;container __ui_search_container__ py-2&quot;&gt;    &lt;div class=&quot;row&quot;&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;    &lt;form id=&quot;__delta_init_basic__&quot;&gt;        &lt;div class=&quot;input-group mb-3&quot;&gt;            &lt;input name=&quot;__ui_search_input__&quot; id=&quot;__ui_search_input__$optIndex&quot; type=&quot;search&quot; data-min-length=&quot;3&quot;                class=&quot;form-control&quot;required placeholder=&quot;Search&quot; aria-label=&quot;Search&quot; aria-describedby=&quot;basic-addon2&quot; /&gt;            &lt;div class=&quot;input-group-append&quot;&gt;                &lt;button id=&quot;__ui_search_button__$optIndex&quot; data-form-id=&quot;__delta_init_basic__&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot;                    data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot;                    data-column='[&quot;schemaName&quot;]' data-page='$page' data-class='ApprovalSequenceSchema'                    class=&quot;btn btn-outline-primary btn-perform-search&quot; type=&quot;button&quot;      data-search-input=&quot;text&quot; data-search-input-id=&quot;__ui_search_input__$optIndex&quot; data-toggle=&quot;tooltip&quot;                    title=&quot;This is a basic search&quot;&gt;Search&lt;/button&gt;            &lt;/div&gt;        &lt;/div&gt;    &lt;/form&gt;&lt;/div&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;&lt;button type=&quot;button &quot;class=&quot;btn btn-outline-primary btn-block&quot; name=&quot;__ui_advanced_search_button__&quot; id=&quot;__ui_advanced_search_button__$optIndex&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot; data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot; data-column='[&quot;schemaId&quot;,&quot;schemaName&quot;,&quot;operationCode&quot;,&quot;approvedJobList&quot;,&quot;approvingJobList&quot;,&quot;validity&quot;,&quot;defaultApproveText&quot;,&quot;extraFilter&quot;,&quot;extraInformation&quot;,&quot;flags&quot;]' data-min-length=&quot;3&quot; data-page='$page' data-class='ApprovalSequenceSchema' data-search-dialog=&quot;__dialog_search_container_01__&quot; data-toggle=&quot;tooltip&quot; title=&quot;This is a more advanced search technique&quot;&gt;Advanced Search&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;&lt;br/&gt;&lt;div id=&quot;__ui_search_error__$optIndex&quot; class=&quot;p-1 ui-sys-error-message&quot;&gt;&lt;/div&gt;&lt;div style=&quot;overflow-x: scroll;&quot; id=&quot;__ui_search_output_target__$optIndex&quot;&gt;&lt;/div&gt;&lt;/div&gt;";
		$line .= "&lt;script type=&quot;text/javascript&quot;&gt;(function($)    {    $(function()    {        var callbackFunction$optIndex = function(\$button1, data, textStatus, optionArgumentArray1) {            var \$dialog1 = $('#' + \$button1.data('searchDialog'));            \$dialog1 = showAdvancedSearchDialog(\$button1, \$dialog1, data, Constant);            \$dialog1.modal('show');      };        $('#__ui_advanced_search_button__$optIndex').on('click', function(e)   {            var \$button1 = $(this);            var columnList = \$button1.data('column');            var classname = \$button1.data('class');            var payload = { columns: columnList, classname: classname };            fSendAjax(\$button1,                    $('&lt;span/&gt;'),                    '../server/serviceGetAdvancedSearchPayload.php',                    payload,                    null,                    null,                    callbackFunction$optIndex,                    null,                    null,                    &quot;POST&quot;,                    true,                    false,                    &quot;Processing ....&quot;,                    null,                    null);        });    });})(jQuery);&lt;/script&gt;";
		return htmlspecialchars_decode($line);
	}
	public function getMyPropertyValue($pname)  {
		if ($pname == "schemaId") return $this->schemaId;
		else if ($pname == "schemaName") return $this->schemaName;
		else if ($pname == "operationCode") return $this->operationCode;
		else if ($pname == "approvedJobList") return $this->approvedJobList;
		else if ($pname == "approvingJobList") return $this->approvingJobList;
		else if ($pname == "validity") return $this->validity;
		else if ($pname == "defaultApproveText") return $this->defaultApproveText;
		else if ($pname == "extraFilter") return $this->extraFilter;
		else if ($pname == "extraInformation") return $this->extraInformation;
		else if ($pname == "flags") return $this->flags;
		return null;
	}
	public static function getValue0Columnname() {
		return "schemaName";
	}
	public static function getValueColumnnames()   {
		return array('schemaName');
	}
	public function getName() {
		return array($this->schemaName);
	}
	public function getName0() {
		$namedValue = $this->schemaName;
		return $namedValue;
	}
}
?>