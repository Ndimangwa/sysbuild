<?php
class Registry extends __object__ {
	public static function getObjectReference($__database, $__conn, $__classname, $__id) {
		$refObj = null;
		if ($__classname == "SystemPolicy") $refObj = new SystemPolicy($__database, $__id, $__conn);
		else if ($__classname == "DaysOfAWeek") $refObj = new DaysOfAWeek($__database, $__id, $__conn);
		else if ($__classname == "MonthOfAYear") $refObj = new MonthOfAYear($__database, $__id, $__conn);
		else if ($__classname == "Theme") $refObj = new Theme($__database, $__id, $__conn);
		else if ($__classname == "Language") $refObj = new Language($__database, $__id, $__conn);
		else if ($__classname == "Country") $refObj = new Country($__database, $__id, $__conn);
		else if ($__classname == "PHPTimezone") $refObj = new PHPTimezone($__database, $__id, $__conn);
		else if ($__classname == "Profile") $refObj = new Profile($__database, $__id, $__conn);
		else if ($__classname == "Group") $refObj = new Group($__database, $__id, $__conn);
		else if ($__classname == "JobTitle") $refObj = new JobTitle($__database, $__id, $__conn);
		else if ($__classname == "Sex") $refObj = new Sex($__database, $__id, $__conn);
		else if ($__classname == "Marital") $refObj = new Marital($__database, $__id, $__conn);
		else if ($__classname == "UserStatus") $refObj = new UserStatus($__database, $__id, $__conn);
		else if ($__classname == "SecurityQuestion") $refObj = new SecurityQuestion($__database, $__id, $__conn);
		else if ($__classname == "Login") $refObj = new Login($__database, $__id, $__conn);
		else if ($__classname == "ApprovalSequenceSchema") $refObj = new ApprovalSequenceSchema($__database, $__id, $__conn);
		else if ($__classname == "ApprovalSequenceData") $refObj = new ApprovalSequenceData($__database, $__id, $__conn);
		return $refObj;
	}
	public static function getTablename($__classname)  {
		$tname = null;
		if ($__classname == "SystemPolicy") $tname = SystemPolicy::getTablename();
		else if ($__classname == "DaysOfAWeek") $tname = DaysOfAWeek::getTablename();
		else if ($__classname == "MonthOfAYear") $tname = MonthOfAYear::getTablename();
		else if ($__classname == "Theme") $tname = Theme::getTablename();
		else if ($__classname == "Language") $tname = Language::getTablename();
		else if ($__classname == "Country") $tname = Country::getTablename();
		else if ($__classname == "PHPTimezone") $tname = PHPTimezone::getTablename();
		else if ($__classname == "Profile") $tname = Profile::getTablename();
		else if ($__classname == "Group") $tname = Group::getTablename();
		else if ($__classname == "JobTitle") $tname = JobTitle::getTablename();
		else if ($__classname == "Sex") $tname = Sex::getTablename();
		else if ($__classname == "Marital") $tname = Marital::getTablename();
		else if ($__classname == "UserStatus") $tname = UserStatus::getTablename();
		else if ($__classname == "SecurityQuestion") $tname = SecurityQuestion::getTablename();
		else if ($__classname == "Login") $tname = Login::getTablename();
		else if ($__classname == "ApprovalSequenceSchema") $tname = ApprovalSequenceSchema::getTablename();
		else if ($__classname == "ApprovalSequenceData") $tname = ApprovalSequenceData::getTablename();
		return $tname;
	}
	public static function getUIControlValidations($__classname, $__columnname, $__controltype = "text")  {
		$uirule = null;
		$regexArray1 = null; $maxLength = null;
		if ($__classname == "SystemPolicy") {
			$regexArray1 = SystemPolicy::getRegularExpression($__columnname);
			$maxLength = SystemPolicy::getMaximumLength($__columnname);
		} else if ($__classname == "DaysOfAWeek") {
			$regexArray1 = DaysOfAWeek::getRegularExpression($__columnname);
			$maxLength = DaysOfAWeek::getMaximumLength($__columnname);
		} else if ($__classname == "MonthOfAYear") {
			$regexArray1 = MonthOfAYear::getRegularExpression($__columnname);
			$maxLength = MonthOfAYear::getMaximumLength($__columnname);
		} else if ($__classname == "Theme") {
			$regexArray1 = Theme::getRegularExpression($__columnname);
			$maxLength = Theme::getMaximumLength($__columnname);
		} else if ($__classname == "Language") {
			$regexArray1 = Language::getRegularExpression($__columnname);
			$maxLength = Language::getMaximumLength($__columnname);
		} else if ($__classname == "Country") {
			$regexArray1 = Country::getRegularExpression($__columnname);
			$maxLength = Country::getMaximumLength($__columnname);
		} else if ($__classname == "PHPTimezone") {
			$regexArray1 = PHPTimezone::getRegularExpression($__columnname);
			$maxLength = PHPTimezone::getMaximumLength($__columnname);
		} else if ($__classname == "Profile") {
			$regexArray1 = Profile::getRegularExpression($__columnname);
			$maxLength = Profile::getMaximumLength($__columnname);
		} else if ($__classname == "Group") {
			$regexArray1 = Group::getRegularExpression($__columnname);
			$maxLength = Group::getMaximumLength($__columnname);
		} else if ($__classname == "JobTitle") {
			$regexArray1 = JobTitle::getRegularExpression($__columnname);
			$maxLength = JobTitle::getMaximumLength($__columnname);
		} else if ($__classname == "Sex") {
			$regexArray1 = Sex::getRegularExpression($__columnname);
			$maxLength = Sex::getMaximumLength($__columnname);
		} else if ($__classname == "Marital") {
			$regexArray1 = Marital::getRegularExpression($__columnname);
			$maxLength = Marital::getMaximumLength($__columnname);
		} else if ($__classname == "UserStatus") {
			$regexArray1 = UserStatus::getRegularExpression($__columnname);
			$maxLength = UserStatus::getMaximumLength($__columnname);
		} else if ($__classname == "SecurityQuestion") {
			$regexArray1 = SecurityQuestion::getRegularExpression($__columnname);
			$maxLength = SecurityQuestion::getMaximumLength($__columnname);
		} else if ($__classname == "Login") {
			$regexArray1 = Login::getRegularExpression($__columnname);
			$maxLength = Login::getMaximumLength($__columnname);
		} else if ($__classname == "ApprovalSequenceSchema") {
			$regexArray1 = ApprovalSequenceSchema::getRegularExpression($__columnname);
			$maxLength = ApprovalSequenceSchema::getMaximumLength($__columnname);
		} else if ($__classname == "ApprovalSequenceData") {
			$regexArray1 = ApprovalSequenceData::getRegularExpression($__columnname);
			$maxLength = ApprovalSequenceData::getMaximumLength($__columnname);
		}
		if (! is_null($regexArray1)) { $rule = $regexArray1['rule']; $message = $regexArray1['message']; $uirule = "data-validation=\"true\" data-validation-control=\"$__controltype\" data-validation-expression=\"$rule\" data-validation-message=\"$message\""; }
		if (! is_null($maxLength)) { $dt = "data-max-length = \"$maxLength\""; if (is_null($uirule))  $uirule = $dt; else $uirule .= " ".$dt; }
		if (is_null($uirule)) $uirule = "";
		return $uirule;
	}
	public static function column2Property($__classname, $__colname)    {
		$pname = null;
		if ($__classname == "SystemPolicy") {
			$pname = SystemPolicy::column2Property($__colname);
		} else if ($__classname == "DaysOfAWeek") {
			$pname = DaysOfAWeek::column2Property($__colname);
		}	 else if ($__classname == "MonthOfAYear") {
			$pname = MonthOfAYear::column2Property($__colname);
		}	 else if ($__classname == "Theme") {
			$pname = Theme::column2Property($__colname);
		}	 else if ($__classname == "Language") {
			$pname = Language::column2Property($__colname);
		}	 else if ($__classname == "Country") {
			$pname = Country::column2Property($__colname);
		}	 else if ($__classname == "PHPTimezone") {
			$pname = PHPTimezone::column2Property($__colname);
		}	 else if ($__classname == "Profile") {
			$pname = Profile::column2Property($__colname);
		}	 else if ($__classname == "Group") {
			$pname = Group::column2Property($__colname);
		}	 else if ($__classname == "JobTitle") {
			$pname = JobTitle::column2Property($__colname);
		}	 else if ($__classname == "Sex") {
			$pname = Sex::column2Property($__colname);
		}	 else if ($__classname == "Marital") {
			$pname = Marital::column2Property($__colname);
		}	 else if ($__classname == "UserStatus") {
			$pname = UserStatus::column2Property($__colname);
		}	 else if ($__classname == "SecurityQuestion") {
			$pname = SecurityQuestion::column2Property($__colname);
		}	 else if ($__classname == "Login") {
			$pname = Login::column2Property($__colname);
		}	 else if ($__classname == "ApprovalSequenceSchema") {
			$pname = ApprovalSequenceSchema::column2Property($__colname);
		}	 else if ($__classname == "ApprovalSequenceData") {
			$pname = ApprovalSequenceData::column2Property($__colname);
		}	
		return $pname;
	}
	public static function property2column($__classname, $__pname)  {
		$colname = null;
		if ($__classname == "SystemPolicy") {
			$colname = SystemPolicy::property2Column($__pname);
		} else if ($__classname == "DaysOfAWeek") {
			$colname = DaysOfAWeek::property2Column($__pname);
		}	 else if ($__classname == "MonthOfAYear") {
			$colname = MonthOfAYear::property2Column($__pname);
		}	 else if ($__classname == "Theme") {
			$colname = Theme::property2Column($__pname);
		}	 else if ($__classname == "Language") {
			$colname = Language::property2Column($__pname);
		}	 else if ($__classname == "Country") {
			$colname = Country::property2Column($__pname);
		}	 else if ($__classname == "PHPTimezone") {
			$colname = PHPTimezone::property2Column($__pname);
		}	 else if ($__classname == "Profile") {
			$colname = Profile::property2Column($__pname);
		}	 else if ($__classname == "Group") {
			$colname = Group::property2Column($__pname);
		}	 else if ($__classname == "JobTitle") {
			$colname = JobTitle::property2Column($__pname);
		}	 else if ($__classname == "Sex") {
			$colname = Sex::property2Column($__pname);
		}	 else if ($__classname == "Marital") {
			$colname = Marital::property2Column($__pname);
		}	 else if ($__classname == "UserStatus") {
			$colname = UserStatus::property2Column($__pname);
		}	 else if ($__classname == "SecurityQuestion") {
			$colname = SecurityQuestion::property2Column($__pname);
		}	 else if ($__classname == "Login") {
			$colname = Login::property2Column($__pname);
		}	 else if ($__classname == "ApprovalSequenceSchema") {
			$colname = ApprovalSequenceSchema::property2Column($__pname);
		}	 else if ($__classname == "ApprovalSequenceData") {
			$colname = ApprovalSequenceData::property2Column($__pname);
		}	
		return $colname;
	}
	public static function loadAllData($__conn, $__classname)   {
		$dataArray1 = null;
		if ($__classname == "SystemPolicy") {
			$dataArray1 = SystemPolicy::loadAllData($__conn);
		} else if ($__classname == "DaysOfAWeek") {
			$dataArray1 = DaysOfAWeek::loadAllData($__conn);
		} else if ($__classname == "MonthOfAYear") {
			$dataArray1 = MonthOfAYear::loadAllData($__conn);
		} else if ($__classname == "Theme") {
			$dataArray1 = Theme::loadAllData($__conn);
		} else if ($__classname == "Language") {
			$dataArray1 = Language::loadAllData($__conn);
		} else if ($__classname == "Country") {
			$dataArray1 = Country::loadAllData($__conn);
		} else if ($__classname == "PHPTimezone") {
			$dataArray1 = PHPTimezone::loadAllData($__conn);
		} else if ($__classname == "Profile") {
			$dataArray1 = Profile::loadAllData($__conn);
		} else if ($__classname == "Group") {
			$dataArray1 = Group::loadAllData($__conn);
		} else if ($__classname == "JobTitle") {
			$dataArray1 = JobTitle::loadAllData($__conn);
		} else if ($__classname == "Sex") {
			$dataArray1 = Sex::loadAllData($__conn);
		} else if ($__classname == "Marital") {
			$dataArray1 = Marital::loadAllData($__conn);
		} else if ($__classname == "UserStatus") {
			$dataArray1 = UserStatus::loadAllData($__conn);
		} else if ($__classname == "SecurityQuestion") {
			$dataArray1 = SecurityQuestion::loadAllData($__conn);
		} else if ($__classname == "Login") {
			$dataArray1 = Login::loadAllData($__conn);
		} else if ($__classname == "ApprovalSequenceSchema") {
			$dataArray1 = ApprovalSequenceSchema::loadAllData($__conn);
		}
		return $dataArray1;
	}
	public static function getReferenceClass($__classname, $__pname)   {
		$refclass = null;
		if ($__classname == "SystemPolicy") {
			$refclass = SystemPolicy::getReferenceClass($__pname);
		} else if ($__classname == "DaysOfAWeek") {
			$refclass = DaysOfAWeek::getReferenceClass($__pname);
		}	 else if ($__classname == "MonthOfAYear") {
			$refclass = MonthOfAYear::getReferenceClass($__pname);
		}	 else if ($__classname == "Theme") {
			$refclass = Theme::getReferenceClass($__pname);
		}	 else if ($__classname == "Language") {
			$refclass = Language::getReferenceClass($__pname);
		}	 else if ($__classname == "Country") {
			$refclass = Country::getReferenceClass($__pname);
		}	 else if ($__classname == "PHPTimezone") {
			$refclass = PHPTimezone::getReferenceClass($__pname);
		}	 else if ($__classname == "Profile") {
			$refclass = Profile::getReferenceClass($__pname);
		}	 else if ($__classname == "Group") {
			$refclass = Group::getReferenceClass($__pname);
		}	 else if ($__classname == "JobTitle") {
			$refclass = JobTitle::getReferenceClass($__pname);
		}	 else if ($__classname == "Sex") {
			$refclass = Sex::getReferenceClass($__pname);
		}	 else if ($__classname == "Marital") {
			$refclass = Marital::getReferenceClass($__pname);
		}	 else if ($__classname == "UserStatus") {
			$refclass = UserStatus::getReferenceClass($__pname);
		}	 else if ($__classname == "SecurityQuestion") {
			$refclass = SecurityQuestion::getReferenceClass($__pname);
		}	 else if ($__classname == "Login") {
			$refclass = Login::getReferenceClass($__pname);
		}	 else if ($__classname == "ApprovalSequenceSchema") {
			$refclass = ApprovalSequenceSchema::getReferenceClass($__pname);
		}	 else if ($__classname == "ApprovalSequenceData") {
			$refclass = ApprovalSequenceData::getReferenceClass($__pname);
		}	
		return $refclass;
	}
	public static function getId0Columnname($__classname)    {
		$colname = "";
		if ($__classname == "SystemPolicy") {
			$colname = SystemPolicy::getId0Columnname();
		} else if ($__classname == "DaysOfAWeek") {
			$colname = DaysOfAWeek::getId0Columnname();
		}	 else if ($__classname == "MonthOfAYear") {
			$colname = MonthOfAYear::getId0Columnname();
		}	 else if ($__classname == "Theme") {
			$colname = Theme::getId0Columnname();
		}	 else if ($__classname == "Language") {
			$colname = Language::getId0Columnname();
		}	 else if ($__classname == "Country") {
			$colname = Country::getId0Columnname();
		}	 else if ($__classname == "PHPTimezone") {
			$colname = PHPTimezone::getId0Columnname();
		}	 else if ($__classname == "Profile") {
			$colname = Profile::getId0Columnname();
		}	 else if ($__classname == "Group") {
			$colname = Group::getId0Columnname();
		}	 else if ($__classname == "JobTitle") {
			$colname = JobTitle::getId0Columnname();
		}	 else if ($__classname == "Sex") {
			$colname = Sex::getId0Columnname();
		}	 else if ($__classname == "Marital") {
			$colname = Marital::getId0Columnname();
		}	 else if ($__classname == "UserStatus") {
			$colname = UserStatus::getId0Columnname();
		}	 else if ($__classname == "SecurityQuestion") {
			$colname = SecurityQuestion::getId0Columnname();
		}	 else if ($__classname == "Login") {
			$colname = Login::getId0Columnname();
		}	 else if ($__classname == "ApprovalSequenceSchema") {
			$colname = ApprovalSequenceSchema::getId0Columnname();
		}	 else if ($__classname == "ApprovalSequenceData") {
			$colname = ApprovalSequenceData::getId0Columnname();
		}	
		return $colname;
	}
	public static function getIdColumnnames($__classname)  {
		$colname = null;
		if ($__classname == "SystemPolicy") $colname = SystemPolicy::getIdColumnnames();
		else if ($__classname == "DaysOfAWeek") $colname = DaysOfAWeek::getIdColumnnames();
		else if ($__classname == "MonthOfAYear") $colname = MonthOfAYear::getIdColumnnames();
		else if ($__classname == "Theme") $colname = Theme::getIdColumnnames();
		else if ($__classname == "Language") $colname = Language::getIdColumnnames();
		else if ($__classname == "Country") $colname = Country::getIdColumnnames();
		else if ($__classname == "PHPTimezone") $colname = PHPTimezone::getIdColumnnames();
		else if ($__classname == "Profile") $colname = Profile::getIdColumnnames();
		else if ($__classname == "Group") $colname = Group::getIdColumnnames();
		else if ($__classname == "JobTitle") $colname = JobTitle::getIdColumnnames();
		else if ($__classname == "Sex") $colname = Sex::getIdColumnnames();
		else if ($__classname == "Marital") $colname = Marital::getIdColumnnames();
		else if ($__classname == "UserStatus") $colname = UserStatus::getIdColumnnames();
		else if ($__classname == "SecurityQuestion") $colname = SecurityQuestion::getIdColumnnames();
		else if ($__classname == "Login") $colname = Login::getIdColumnnames();
		else if ($__classname == "ApprovalSequenceSchema") $colname = ApprovalSequenceSchema::getIdColumnnames();
		else if ($__classname == "ApprovalSequenceData") $colname = ApprovalSequenceData::getIdColumnnames();
		return $colname;
	}
	public static function getColumnType($__classname, $__pname) {
		$coltype = null;
		if ($__classname == "SystemPolicy") $coltype = SystemPolicy::getColumnType($__pname);
		else if ($__classname == "DaysOfAWeek") $coltype = DaysOfAWeek::getColumnType($__pname);
		else if ($__classname == "MonthOfAYear") $coltype = MonthOfAYear::getColumnType($__pname);
		else if ($__classname == "Theme") $coltype = Theme::getColumnType($__pname);
		else if ($__classname == "Language") $coltype = Language::getColumnType($__pname);
		else if ($__classname == "Country") $coltype = Country::getColumnType($__pname);
		else if ($__classname == "PHPTimezone") $coltype = PHPTimezone::getColumnType($__pname);
		else if ($__classname == "Profile") $coltype = Profile::getColumnType($__pname);
		else if ($__classname == "Group") $coltype = Group::getColumnType($__pname);
		else if ($__classname == "JobTitle") $coltype = JobTitle::getColumnType($__pname);
		else if ($__classname == "Sex") $coltype = Sex::getColumnType($__pname);
		else if ($__classname == "Marital") $coltype = Marital::getColumnType($__pname);
		else if ($__classname == "UserStatus") $coltype = UserStatus::getColumnType($__pname);
		else if ($__classname == "SecurityQuestion") $coltype = SecurityQuestion::getColumnType($__pname);
		else if ($__classname == "Login") $coltype = Login::getColumnType($__pname);
		else if ($__classname == "ApprovalSequenceSchema") $coltype = ApprovalSequenceSchema::getColumnType($__pname);
		else if ($__classname == "ApprovalSequenceData") $coltype = ApprovalSequenceData::getColumnType($__pname);
		return $coltype;
	}
	public static function getSearchableColumns($__classname) {
		$pname = null;
		if ($__classname == "SystemPolicy") $pname = SystemPolicy::getSearchableColumns();
		else if ($__classname == "DaysOfAWeek") $pname = DaysOfAWeek::getSearchableColumns();
		else if ($__classname == "MonthOfAYear") $pname = MonthOfAYear::getSearchableColumns();
		else if ($__classname == "Theme") $pname = Theme::getSearchableColumns();
		else if ($__classname == "Language") $pname = Language::getSearchableColumns();
		else if ($__classname == "Country") $pname = Country::getSearchableColumns();
		else if ($__classname == "PHPTimezone") $pname = PHPTimezone::getSearchableColumns();
		else if ($__classname == "Profile") $pname = Profile::getSearchableColumns();
		else if ($__classname == "Group") $pname = Group::getSearchableColumns();
		else if ($__classname == "JobTitle") $pname = JobTitle::getSearchableColumns();
		else if ($__classname == "Sex") $pname = Sex::getSearchableColumns();
		else if ($__classname == "Marital") $pname = Marital::getSearchableColumns();
		else if ($__classname == "UserStatus") $pname = UserStatus::getSearchableColumns();
		else if ($__classname == "SecurityQuestion") $pname = SecurityQuestion::getSearchableColumns();
		else if ($__classname == "Login") $pname = Login::getSearchableColumns();
		else if ($__classname == "ApprovalSequenceSchema") $pname = ApprovalSequenceSchema::getSearchableColumns();
		else if ($__classname == "ApprovalSequenceData") $pname = ApprovalSequenceData::getSearchableColumns();
		return $pname;
	}
	public static function columnTransitiveMap($__classname, $__pname)    {
		$pmap = null;
		if ($__classname == "SystemPolicy") $pmap = SystemPolicy::columnTransitiveMap($__pname);
		else if ($__classname == "DaysOfAWeek") $pmap = DaysOfAWeek::columnTransitiveMap($__pname);
		else if ($__classname == "MonthOfAYear") $pmap = MonthOfAYear::columnTransitiveMap($__pname);
		else if ($__classname == "Theme") $pmap = Theme::columnTransitiveMap($__pname);
		else if ($__classname == "Language") $pmap = Language::columnTransitiveMap($__pname);
		else if ($__classname == "Country") $pmap = Country::columnTransitiveMap($__pname);
		else if ($__classname == "PHPTimezone") $pmap = PHPTimezone::columnTransitiveMap($__pname);
		else if ($__classname == "Profile") $pmap = Profile::columnTransitiveMap($__pname);
		else if ($__classname == "Group") $pmap = Group::columnTransitiveMap($__pname);
		else if ($__classname == "JobTitle") $pmap = JobTitle::columnTransitiveMap($__pname);
		else if ($__classname == "Sex") $pmap = Sex::columnTransitiveMap($__pname);
		else if ($__classname == "Marital") $pmap = Marital::columnTransitiveMap($__pname);
		else if ($__classname == "UserStatus") $pmap = UserStatus::columnTransitiveMap($__pname);
		else if ($__classname == "SecurityQuestion") $pmap = SecurityQuestion::columnTransitiveMap($__pname);
		else if ($__classname == "Login") $pmap = Login::columnTransitiveMap($__pname);
		else if ($__classname == "ApprovalSequenceSchema") $pmap = ApprovalSequenceSchema::columnTransitiveMap($__pname);
		else if ($__classname == "ApprovalSequenceData") $pmap = ApprovalSequenceData::columnTransitiveMap($__pname);
		return $pmap;
	}
	public static function getValue0Columnname($__classname)  {
		$colname = null;
		if ($__classname == "SystemPolicy") $colname = SystemPolicy::getValue0Columnname();
		else if ($__classname == "DaysOfAWeek") $colname = DaysOfAWeek::getValue0Columnname();
		else if ($__classname == "MonthOfAYear") $colname = MonthOfAYear::getValue0Columnname();
		else if ($__classname == "Theme") $colname = Theme::getValue0Columnname();
		else if ($__classname == "Language") $colname = Language::getValue0Columnname();
		else if ($__classname == "Country") $colname = Country::getValue0Columnname();
		else if ($__classname == "PHPTimezone") $colname = PHPTimezone::getValue0Columnname();
		else if ($__classname == "Profile") $colname = Profile::getValue0Columnname();
		else if ($__classname == "Group") $colname = Group::getValue0Columnname();
		else if ($__classname == "JobTitle") $colname = JobTitle::getValue0Columnname();
		else if ($__classname == "Sex") $colname = Sex::getValue0Columnname();
		else if ($__classname == "Marital") $colname = Marital::getValue0Columnname();
		else if ($__classname == "UserStatus") $colname = UserStatus::getValue0Columnname();
		else if ($__classname == "SecurityQuestion") $colname = SecurityQuestion::getValue0Columnname();
		else if ($__classname == "Login") $colname = Login::getValue0Columnname();
		else if ($__classname == "ApprovalSequenceSchema") $colname = ApprovalSequenceSchema::getValue0Columnname();
		else if ($__classname == "ApprovalSequenceData") $colname = ApprovalSequenceData::getValue0Columnname();
		return $colname;
	}
	public static function getValueColumnnames($__classname)  {
		$colname = null;
		if ($__classname == "SystemPolicy") $colname = SystemPolicy::getValueColumnnames();
		else if ($__classname == "DaysOfAWeek") $colname = DaysOfAWeek::getValueColumnnames();
		else if ($__classname == "MonthOfAYear") $colname = MonthOfAYear::getValueColumnnames();
		else if ($__classname == "Theme") $colname = Theme::getValueColumnnames();
		else if ($__classname == "Language") $colname = Language::getValueColumnnames();
		else if ($__classname == "Country") $colname = Country::getValueColumnnames();
		else if ($__classname == "PHPTimezone") $colname = PHPTimezone::getValueColumnnames();
		else if ($__classname == "Profile") $colname = Profile::getValueColumnnames();
		else if ($__classname == "Group") $colname = Group::getValueColumnnames();
		else if ($__classname == "JobTitle") $colname = JobTitle::getValueColumnnames();
		else if ($__classname == "Sex") $colname = Sex::getValueColumnnames();
		else if ($__classname == "Marital") $colname = Marital::getValueColumnnames();
		else if ($__classname == "UserStatus") $colname = UserStatus::getValueColumnnames();
		else if ($__classname == "SecurityQuestion") $colname = SecurityQuestion::getValueColumnnames();
		else if ($__classname == "Login") $colname = Login::getValueColumnnames();
		else if ($__classname == "ApprovalSequenceSchema") $colname = ApprovalSequenceSchema::getValueColumnnames();
		else if ($__classname == "ApprovalSequenceData") $colname = ApprovalSequenceData::getValueColumnnames();
		return $colname;
	}
}
?>