<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['login'][0]['id'])) {
    header("Location: ../");
    exit();
}
require_once("../common/__autoload__.php"); //common for both ui and back-end
require_once("../sys/__autoload__.php");
$conn = null; //Since this is a multi-page environment, you have to close the connection immediately after using, due to static-methods which initiate connections themselves
$profile1 = null;
$login1 = null;
$config1 = new ConfigurationData("../config.php");
$host = $config1->getHostname();
$dbname = $config1->getDatabase();
try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $config1->getUsername(), $config1->getPassword());
    $profile1 = new Profile($dbname, __data__::$__PROFILE_INIT_ID, $conn);
    $login1 = new Login($dbname, $_SESSION['login'][0]['id'], $conn);
} catch (Exception $e) {
    die($e->getMessage()); //Prepare a safer landing page
}
$conn = null;
date_default_timezone_set($profile1->getPHPTimezone()->getZoneName());
$systemTime1 = new DateAndTime(date("Y:m:d:H:i:s"));
//page-navigation
$thispage = $_SERVER['PHP_SELF'];
$page = null;
if (isset($_REQUEST['page'])) $page = $_REQUEST['page'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $profile1->getProfileName() ?></title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="../css/style.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script src="../common/constants.js" type="text/javascript"></script>
    <script src="../js/mysystem.js" type="text/javascript"></script>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <!-- <div class="container"> -->
    <!--START HERE-->

    <!--NAVBAR-->
    <?php include('../pages/mainnavbar.php'); ?>

    <!--ACTIONS-->
    <?php
    if ($page == "firewall_read" && isset($_GET['class']) && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/firewall_read.php");
    } else if ($page == "firewall_update" && isset($_GET['class']) && isset($_GET['id']) && $login1->isRoot()) {
        include("../pages/firewall_update.php");
    } else if ($page == "group_delete" && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/group_delete.php");
    } else if ($page == "group_read" && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/group_read.php");
    } else if ($page == "group_update" && isset($_GET['id']) && $login1->isRoot()) {
        include("../pages/group_update.php");
    } else if ($page == "group_create" && $login1->isRoot())  {
        include("../pages/group_create.php");
    } else if ($page == "group" && $login1->isRoot()) {
        //Note User-Data are carried in Login table
        include("../pages/group.php");
    } else if ($page == "jobtitle_delete" && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/jobtitle_delete.php");
    } else if ($page == "jobtitle_read" && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/jobtitle_read.php");
    } else if ($page == "jobtitle_update" && isset($_GET['id']) && $login1->isRoot()) {
        include("../pages/jobtitle_update.php");
    } else if ($page == "jobtitle_create" && $login1->isRoot())  {
        include("../pages/jobtitle_create.php");
    } else if ($page == "jobtitle" && $login1->isRoot()) {
        //Note User-Data are carried in Login table
        include("../pages/jobtitle.php");
    } else if ($page == "login_delete" && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/login_delete.php");
    } else if ($page == "login_read" && isset($_GET['id']) && $login1->isRoot())    {
        include("../pages/login_read.php");
    } else if ($page == "login_update" && isset($_GET['id']) && $login1->isRoot()) {
        include("../pages/login_update.php");
    } else if ($page == "login_create" && $login1->isRoot())  {
        include("../pages/login_create.php");
    }else if ($page == "login" && $login1->isRoot()) {
        //Note User-Data are carried in Login table
        include("../pages/login.php");
    } else if ($page == "systemlogs" && $login1->isRoot())    {
        include("../pages/systemlogs.php");
    } else if ($page == "lastresortdonotcare" && $login1->isRoot()) {
        include("../pages/lastresortdonotcare.php");
    } else if ($page == "update_my_login" && Authorize::isAllowable($config1, "update_my_login", "normal", "setlog", null, null)) {
        include("../pages/updatemylogin.php");
    } else if ($page == "profile_update" && $login1->isRoot()) {
        include("../pages/profileUpdate.php");
    } else { /*BEGIN : OPERATION DENIED*/
        if (Authorize::isSessionSet()) {
            include("../pages/operationDenied.php");
        } /*END : OPERATION DENIED*/
        echo "<h1>DASHBOARD UI</h1>";
    }
    ?>
    <!--FOOTER-->
    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="p lead text-center">
                        Copyright &copy; <span id="year"></span> <?= $profile1->getProfileName() ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--MODALS-->
    <!--QUICK SUBMISSION MODAL-->
    <div class="modal fade" id="__status_query_modal__" data-secondary-modal="__status_query_modal__02" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Default Modal Title</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-primary dialog-save-button" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="__status_query_modal__02" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Default Modal Title</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-primary dialog-save-button" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!--ADVANCED QUERY COLUMNS EXECUTOR-->
    <div class="modal fade" id="__dialog_search_container_01__" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Default Modal Title</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-outline-warning" type="button" data-dismiss="modal">Close</button>
                    &nbsp;&nbsp; <button class="btn btn-outline-primary btn-dialog-search" type="button" data-dismiss="modal">Search Data</button>
                </div>
            </div>
        </div>
    </div>

    <!--END HERE-->
    <!-- </div> -->
    <script type="text/javascript">
        $('#year').text(new Date().getFullYear());
        //CKEDITOR.replace( 'editor1' );
        $('#logoutButton').on('click', function(event) {
            event.preventDefault();
            var $button1 = $(this);
            var $errorTarget1 = $('<span/>');
            dataToSend = {
                noAuthenticate: true
            };
            sendAjax(
                $button1,
                $errorTarget1,
                '../server/serviceNoAuthentication.php',
                dataToSend,
                '../',
                null,
                'POST',
                true,
                false
            );
        });
        //DialogAjax
        $('button.btn-send-dialog-ajax').on('click', function(e) {
            e.preventDefault();
            var $button1 = $(this);
            var $form1 = $button1.closest('form');
            var $dialog1 = $('#__status_query_modal__');
            var errorTarget = $button1.data('formError');
            $errorTarget1 = $('#' + errorTarget);
            if ($errorTarget1.length && !generalFormValidation($button1, $form1, $errorTarget1, Constant)) return false;
            var dataToSend = $form1.serializeObject();
            var nextPage = "<?= $thispage ?>?page=<?= $page ?>";
            if ($button1.attr('data-next-page') !== undefined) nextPage = $button1.attr('data-next-page');
            var serverScript = '../server/serviceQueryProcessor.php';
            if ($button1.attr('data-server-script') !== undefined) serverScript = "../server/" + $button1.attr('data-server-script') + '.php';
            sendAjaxDialog(
                $button1,
                $dialog1,
                serverScript,
                dataToSend,
                nextPage,
                null,
                "POST",
                true,
                false,
                "Saving ...",
                "Saved",
                "Retry ..."
            );
        });
        //Change Password
        $('button.btn-change-password-dialog-ajax').on('click', function(e) {
            e.preventDefault();
            var $button1 = $(this);
            var $form1 = $button1.closest('form');
            var $dialog1 = $('#__status_query_modal__');
            var errorTarget = $button1.data('formError');
            $errorTarget1 = $('#' + errorTarget);
            //We need to perform manual validation for Password
            var $oldPassword1 = $('#oldPassword');
            var $newPassword1 = $('#newPassword');
            var $confirmNewPassword1 = $('#confirmNewPassword');
            if (!($oldPassword1.length && $newPassword1.length && $confirmNewPassword1.length)) return false;
            if ($newPassword1.val() != $confirmNewPassword1.val()) {
                $errorTarget1.empty();
                if (!$newPassword1.hasClass('invalid-input')) $newPassword1.addClass('invalid-input');
                if (!$confirmNewPassword1.hasClass('invalid-input')) $confirmNewPassword1.addClass('invalid-input');
                $('<span/>').text('New Password and Confirm New Password does not match').appendTo($errorTarget1);
                return false;
            }
            if ($errorTarget1.length && !generalFormValidation($button1, $form1, $errorTarget1, Constant)) return false;
            var dataToSend = $form1.serializeObject();
            sendAjaxDialog(
                $button1,
                $dialog1,
                '../server/serviceChangePassword.php',
                dataToSend,
                '<?= $thispage ?>?page=<?= $page ?>',
                null,
                "POST",
                true,
                false,
                "Saving ...",
                "Saved",
                "Retry ..."
            );
        });
        //Working on Search
        $('body').on('click', 'button.btn-perform-search', function(e) {
            var $button1 = $(this);
            var $form1 = $('#' + $button1.data('formId'));
            var $errorTarget1 = $('#' + $button1.data('errorTarget'));  
            if (generalFormValidation($button1, $form1, $errorTarget1, Constant)) showSearchTableSection($button1, Constant);
        });
    </script>
</body>

</html>