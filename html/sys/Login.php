<?php
/******************************************************
**                                                   **
**                 CLASSNAME : Login                 **
**  Copyright (c) Zoomtong Company Limited           **
**  Developed by : Ndimangwa Fadhili Ngoya           **
**  Timestamp    : 2021:08:01:21:14:04               **
**  Phones       : +255 787 101 808 / 762 357 596    **
**  Email        : ndimangwa@gmail.com               **
**  Address      : P.O BOX 7436 MOSHI, TANZANIA      **
**                                                   **
**  Dedication to my dear wife Valentina             **
**                my daughters Raheli & Keziah       **
**                                                   **
*******************************************************/
class Login extends __data__ {
	protected $database;
	protected $conn;
	private $loginId;
	private $loginName;
	private $password;
	private $fullName;
	private $group;
	private $sex;
	private $marital;
	private $context;
	private $jobTitle;
	private $root;
	private $initializationCounter;
	private $phone;
	private $email;
	private $status;
	private $photo;
	private $firstSecurityQuestion;
	private $firstSecurityAnswer;
	private $secondSecurityQuestion;
	private $secondSecurityAnswer;
	private $firstDayOfAWeek;
	private $randomNumber;
	private $lastLoginTime;
	private $extraFilter;
	private $extraInformation;
	private $flags;
/*BEGIN OF CUSTOM CODES : You should Add Your Custom Codes Below this line*/

/*END OF CUSTOM CODES : You should Add Your Custom Codes Above this line*/
	public static function create($database, $id, $conn) { return new Login($database, $id, $conn); }
	public function __construct($database, $id, $conn)    {
		$this->setMe($database, $id, $conn);
	}
	public function setMe($database, $id, $conn)    {
		$this->database = $database;
		$this->conn = $conn;
		$whereClause = self::getId0Columnname();
		$whereClause = array($whereClause => $id);
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), array('*'), $whereClause);
		$jresult1 = SQLEngine::execute($query, $conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] !== 1) throw new Exception("Duplicate or no record found");
		$resultSet = $jArray1['rows'][0];
		if (! array_key_exists("loginId", $resultSet)) throw new Exception("Column [loginId] not available while pulling data");
		$this->loginId = $resultSet["loginId"];
		if (! array_key_exists("loginName", $resultSet)) throw new Exception("Column [loginName] not available while pulling data");
		$this->setLoginName($resultSet["loginName"]);
		if (! array_key_exists("password", $resultSet)) throw new Exception("Column [password] not available while pulling data");
		$this->setPassword($resultSet["password"]);
		if (! array_key_exists("fullName", $resultSet)) throw new Exception("Column [fullName] not available while pulling data");
		$this->setFullName($resultSet["fullName"]);
		if (! array_key_exists("groupId", $resultSet)) throw new Exception("Column [groupId] not available while pulling data");
		$this->setGroup($resultSet["groupId"]);
		if (! array_key_exists("sexId", $resultSet)) throw new Exception("Column [sexId] not available while pulling data");
		$this->setSex($resultSet["sexId"]);
		if (! array_key_exists("maritalId", $resultSet)) throw new Exception("Column [maritalId] not available while pulling data");
		$this->setMarital($resultSet["maritalId"]);
		if (! array_key_exists("context", $resultSet)) throw new Exception("Column [context] not available while pulling data");
		$this->setContext($resultSet["context"]);
		if (! array_key_exists("jobId", $resultSet)) throw new Exception("Column [jobId] not available while pulling data");
		$this->setJobTitle($resultSet["jobId"]);
		if (! array_key_exists("root", $resultSet)) throw new Exception("Column [root] not available while pulling data");
		$this->setRoot($resultSet["root"]);
		if (! array_key_exists("initializationCounter", $resultSet)) throw new Exception("Column [initializationCounter] not available while pulling data");
		$this->setInitializationCounter($resultSet["initializationCounter"]);
		if (! array_key_exists("phone", $resultSet)) throw new Exception("Column [phone] not available while pulling data");
		$this->setPhone($resultSet["phone"]);
		if (! array_key_exists("email", $resultSet)) throw new Exception("Column [email] not available while pulling data");
		$this->setEmail($resultSet["email"]);
		if (! array_key_exists("statusId", $resultSet)) throw new Exception("Column [statusId] not available while pulling data");
		$this->setStatus($resultSet["statusId"]);
		if (! array_key_exists("photo", $resultSet)) throw new Exception("Column [photo] not available while pulling data");
		$this->setPhoto($resultSet["photo"]);
		if (! array_key_exists("qnSecurity1", $resultSet)) throw new Exception("Column [qnSecurity1] not available while pulling data");
		$this->setFirstSecurityQuestion($resultSet["qnSecurity1"]);
		if (! array_key_exists("ansSecurity1", $resultSet)) throw new Exception("Column [ansSecurity1] not available while pulling data");
		$this->setFirstSecurityAnswer($resultSet["ansSecurity1"]);
		if (! array_key_exists("qnSecurity2", $resultSet)) throw new Exception("Column [qnSecurity2] not available while pulling data");
		$this->setSecondSecurityQuestion($resultSet["qnSecurity2"]);
		if (! array_key_exists("ansSecurity2", $resultSet)) throw new Exception("Column [ansSecurity2] not available while pulling data");
		$this->setSecondSecurityAnswer($resultSet["ansSecurity2"]);
		if (! array_key_exists("firstDayOfAWeekId", $resultSet)) throw new Exception("Column [firstDayOfAWeekId] not available while pulling data");
		$this->setFirstDayOfAWeek($resultSet["firstDayOfAWeekId"]);
		if (! array_key_exists("randomNumber", $resultSet)) throw new Exception("Column [randomNumber] not available while pulling data");
		$this->setRandomNumber($resultSet["randomNumber"]);
		if (! array_key_exists("lastLoginTime", $resultSet)) throw new Exception("Column [lastLoginTime] not available while pulling data");
		$this->setLastLoginTime($resultSet["lastLoginTime"]);
		if (! array_key_exists("extraFilter", $resultSet)) throw new Exception("Column [extraFilter] not available while pulling data");
		$this->setExtraFilter($resultSet["extraFilter"]);
		if (! array_key_exists("extraInformation", $resultSet)) throw new Exception("Column [extraInformation] not available while pulling data");
		$this->setExtraInformation($resultSet["extraInformation"]);
		if (! array_key_exists("flags", $resultSet)) throw new Exception("Column [flags] not available while pulling data");
		$this->setFlags($resultSet["flags"]);
		$this->clearUpdateList();
		return $this;
	}
	public static function loadAllData($__conn) {
		$colArray1 = array('loginId', 'loginName');
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), $colArray1, null);
		$jresult1 = SQLEngine::execute($query, $__conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		$dataArray1 = array();
		foreach ($jArray1['rows'] as $resultSet)    {
			$index = sizeof($dataArray1); $dataArray1[$index] = array();
			$dataArray1[$index]['__id__'] = $resultSet['loginId'];
			$myval = "";
			$myval .= " ".$resultSet['loginName'];
			$dataArray1[$index]['__name__'] = trim($myval);
		}
		return $dataArray1;
	}
	public function getId() { return md5($this->loginId); }
	public function getIdWhereClause() { return "{ \"loginId\" : $this->loginId }"; }
	public function getId0()  { return $this->loginId; }
	public function getId0WhereClause()  { return "{ \"loginId\" : $this->loginId }"; }
	public function getLoginId(){
		return $this->loginId;
	}
	public function setLoginName($loginName){
		$maxLength = self::getMaximumLength('loginName');
		if (! (is_null($maxLength) || ! (strlen($loginName) > $maxLength))) throw new Exception("[ loginName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('loginName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $loginName) === 1)) throw new Exception("[ loginName ] : ".$regex['message']);
		$this->loginName = $loginName;
		$this->addToUpdateList("loginName", $loginName);
		return $this;
	}
	public function getLoginName(){
		return $this->loginName;
	}
	public function setPassword($password){
		$maxLength = self::getMaximumLength('password');
		if (! (is_null($maxLength) || ! (strlen($password) > $maxLength))) throw new Exception("[ password ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('password');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $password) === 1)) throw new Exception("[ password ] : ".$regex['message']);
		$this->password = $password;
		$this->addToUpdateList("password", $password);
		return $this;
	}
	public function getPassword(){
		return $this->password;
	}
	public function setFullName($fullName){
		$maxLength = self::getMaximumLength('fullName');
		if (! (is_null($maxLength) || ! (strlen($fullName) > $maxLength))) throw new Exception("[ fullName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('fullName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $fullName) === 1)) throw new Exception("[ fullName ] : ".$regex['message']);
		$this->fullName = $fullName;
		$this->addToUpdateList("fullName", $fullName);
		return $this;
	}
	public function getFullName(){
		return $this->fullName;
	}
	public function setGroup($groupId){
		$maxLength = self::getMaximumLength('group');
		if (! (is_null($maxLength) || ! (strlen($groupId) > $maxLength))) throw new Exception("[ group ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('group');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $groupId) === 1)) throw new Exception("[ group ] : ".$regex['message']);
		if (is_null($groupId)) return $this;
		$this->group = new Group($this->database, $groupId, $this->conn);
		$this->addToUpdateList("groupId", $groupId);
		return $this;
	}
	public function getGroup(){
		return $this->group;
	}
	public function setSex($sexId){
		$maxLength = self::getMaximumLength('sex');
		if (! (is_null($maxLength) || ! (strlen($sexId) > $maxLength))) throw new Exception("[ sex ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('sex');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $sexId) === 1)) throw new Exception("[ sex ] : ".$regex['message']);
		if (is_null($sexId)) return $this;
		$this->sex = new Sex($this->database, $sexId, $this->conn);
		$this->addToUpdateList("sexId", $sexId);
		return $this;
	}
	public function getSex(){
		return $this->sex;
	}
	public function setMarital($maritalId){
		$maxLength = self::getMaximumLength('marital');
		if (! (is_null($maxLength) || ! (strlen($maritalId) > $maxLength))) throw new Exception("[ marital ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('marital');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $maritalId) === 1)) throw new Exception("[ marital ] : ".$regex['message']);
		if (is_null($maritalId)) return $this;
		$this->marital = new Marital($this->database, $maritalId, $this->conn);
		$this->addToUpdateList("maritalId", $maritalId);
		return $this;
	}
	public function getMarital(){
		return $this->marital;
	}
	public function setContext($context){
		$maxLength = self::getMaximumLength('context');
		if (! (is_null($maxLength) || ! (strlen($context) > $maxLength))) throw new Exception("[ context ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('context');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $context) === 1)) throw new Exception("[ context ] : ".$regex['message']);
		$this->context = $context;
		$this->addToUpdateList("context", $context);
		return $this;
	}
	public function getContext(){
		return $this->context;
	}
	public function setJobTitle($jobId){
		$maxLength = self::getMaximumLength('jobTitle');
		if (! (is_null($maxLength) || ! (strlen($jobId) > $maxLength))) throw new Exception("[ jobTitle ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('jobTitle');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $jobId) === 1)) throw new Exception("[ jobTitle ] : ".$regex['message']);
		if (is_null($jobId)) return $this;
		$this->jobTitle = new JobTitle($this->database, $jobId, $this->conn);
		$this->addToUpdateList("jobId", $jobId);
		return $this;
	}
	public function getJobTitle(){
		return $this->jobTitle;
	}
	public function setRoot($root){
		$maxLength = self::getMaximumLength('root');
		if (! (is_null($maxLength) || ! (strlen($root) > $maxLength))) throw new Exception("[ root ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('root');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $root) === 1)) throw new Exception("[ root ] : ".$regex['message']);
		$this->root = (intval($root) == 1);
		$this->addToUpdateList("root", $root);
		return $this;
	}
	public function isRoot(){
		return $this->root;
	}
	public function setInitializationCounter($initializationCounter){
		$maxLength = self::getMaximumLength('initializationCounter');
		if (! (is_null($maxLength) || ! (strlen($initializationCounter) > $maxLength))) throw new Exception("[ initializationCounter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('initializationCounter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $initializationCounter) === 1)) throw new Exception("[ initializationCounter ] : ".$regex['message']);
		$this->initializationCounter = $initializationCounter;
		$this->addToUpdateList("initializationCounter", $initializationCounter);
		return $this;
	}
	public function getInitializationCounter(){
		return $this->initializationCounter;
	}
	public function setPhone($phone){
		$maxLength = self::getMaximumLength('phone');
		if (! (is_null($maxLength) || ! (strlen($phone) > $maxLength))) throw new Exception("[ phone ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('phone');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $phone) === 1)) throw new Exception("[ phone ] : ".$regex['message']);
		$this->phone = $phone;
		$this->addToUpdateList("phone", $phone);
		return $this;
	}
	public function getPhone(){
		return $this->phone;
	}
	public function setEmail($email){
		$maxLength = self::getMaximumLength('email');
		if (! (is_null($maxLength) || ! (strlen($email) > $maxLength))) throw new Exception("[ email ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('email');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $email) === 1)) throw new Exception("[ email ] : ".$regex['message']);
		$this->email = $email;
		$this->addToUpdateList("email", $email);
		return $this;
	}
	public function getEmail(){
		return $this->email;
	}
	public function setStatus($statusId){
		$maxLength = self::getMaximumLength('status');
		if (! (is_null($maxLength) || ! (strlen($statusId) > $maxLength))) throw new Exception("[ status ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('status');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $statusId) === 1)) throw new Exception("[ status ] : ".$regex['message']);
		if (is_null($statusId)) return $this;
		$this->status = new UserStatus($this->database, $statusId, $this->conn);
		$this->addToUpdateList("statusId", $statusId);
		return $this;
	}
	public function getStatus(){
		return $this->status;
	}
	public function setPhoto($photo){
		$maxLength = self::getMaximumLength('photo');
		if (! (is_null($maxLength) || ! (strlen($photo) > $maxLength))) throw new Exception("[ photo ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('photo');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $photo) === 1)) throw new Exception("[ photo ] : ".$regex['message']);
		$this->photo = $photo;
		$this->addToUpdateList("photo", $photo);
		return $this;
	}
	public function getPhoto(){
		return $this->photo;
	}
	public function setFirstSecurityQuestion($questionId){
		$maxLength = self::getMaximumLength('firstSecurityQuestion');
		if (! (is_null($maxLength) || ! (strlen($questionId) > $maxLength))) throw new Exception("[ firstSecurityQuestion ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('firstSecurityQuestion');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $questionId) === 1)) throw new Exception("[ firstSecurityQuestion ] : ".$regex['message']);
		if (is_null($questionId)) return $this;
		$this->firstSecurityQuestion = new SecurityQuestion($this->database, $questionId, $this->conn);
		$this->addToUpdateList("qnSecurity1", $questionId);
		return $this;
	}
	public function getFirstSecurityQuestion(){
		return $this->firstSecurityQuestion;
	}
	public function setFirstSecurityAnswer($firstSecurityAnswer){
		$maxLength = self::getMaximumLength('firstSecurityAnswer');
		if (! (is_null($maxLength) || ! (strlen($firstSecurityAnswer) > $maxLength))) throw new Exception("[ firstSecurityAnswer ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('firstSecurityAnswer');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $firstSecurityAnswer) === 1)) throw new Exception("[ firstSecurityAnswer ] : ".$regex['message']);
		$this->firstSecurityAnswer = $firstSecurityAnswer;
		$this->addToUpdateList("ansSecurity1", $firstSecurityAnswer);
		return $this;
	}
	public function getFirstSecurityAnswer(){
		return $this->firstSecurityAnswer;
	}
	public function setSecondSecurityQuestion($questionId){
		$maxLength = self::getMaximumLength('secondSecurityQuestion');
		if (! (is_null($maxLength) || ! (strlen($questionId) > $maxLength))) throw new Exception("[ secondSecurityQuestion ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('secondSecurityQuestion');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $questionId) === 1)) throw new Exception("[ secondSecurityQuestion ] : ".$regex['message']);
		if (is_null($questionId)) return $this;
		$this->secondSecurityQuestion = new SecurityQuestion($this->database, $questionId, $this->conn);
		$this->addToUpdateList("qnSecurity2", $questionId);
		return $this;
	}
	public function getSecondSecurityQuestion(){
		return $this->secondSecurityQuestion;
	}
	public function setSecondSecurityAnswer($secondSecurityAnswer){
		$maxLength = self::getMaximumLength('secondSecurityAnswer');
		if (! (is_null($maxLength) || ! (strlen($secondSecurityAnswer) > $maxLength))) throw new Exception("[ secondSecurityAnswer ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('secondSecurityAnswer');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $secondSecurityAnswer) === 1)) throw new Exception("[ secondSecurityAnswer ] : ".$regex['message']);
		$this->secondSecurityAnswer = $secondSecurityAnswer;
		$this->addToUpdateList("ansSecurity2", $secondSecurityAnswer);
		return $this;
	}
	public function getSecondSecurityAnswer(){
		return $this->secondSecurityAnswer;
	}
	public function setFirstDayOfAWeek($dayId){
		$maxLength = self::getMaximumLength('firstDayOfAWeek');
		if (! (is_null($maxLength) || ! (strlen($dayId) > $maxLength))) throw new Exception("[ firstDayOfAWeek ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('firstDayOfAWeek');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $dayId) === 1)) throw new Exception("[ firstDayOfAWeek ] : ".$regex['message']);
		if (is_null($dayId)) return $this;
		$this->firstDayOfAWeek = new DaysOfAWeek($this->database, $dayId, $this->conn);
		$this->addToUpdateList("firstDayOfAWeekId", $dayId);
		return $this;
	}
	public function getFirstDayOfAWeek(){
		return $this->firstDayOfAWeek;
	}
	public function setRandomNumber($randomNumber){
		$maxLength = self::getMaximumLength('randomNumber');
		if (! (is_null($maxLength) || ! (strlen($randomNumber) > $maxLength))) throw new Exception("[ randomNumber ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('randomNumber');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $randomNumber) === 1)) throw new Exception("[ randomNumber ] : ".$regex['message']);
		$this->randomNumber = $randomNumber;
		$this->addToUpdateList("randomNumber", $randomNumber);
		return $this;
	}
	public function getRandomNumber(){
		return $this->randomNumber;
	}
	public function setLastLoginTime($lastLoginTime){
		$maxLength = self::getMaximumLength('lastLoginTime');
		if (! (is_null($maxLength) || ! (strlen($lastLoginTime) > $maxLength))) throw new Exception("[ lastLoginTime ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('lastLoginTime');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $lastLoginTime) === 1)) throw new Exception("[ lastLoginTime ] : ".$regex['message']);
		if (is_null($lastLoginTime)) return $this;
		$this->lastLoginTime = new DateAndTime($lastLoginTime);
		$this->addToUpdateList("lastLoginTime", $lastLoginTime);
		return $this;
	}
	public function getLastLoginTime(){
		return $this->lastLoginTime;
	}
	public function setExtraFilter($extraFilter){
		$maxLength = self::getMaximumLength('extraFilter');
		if (! (is_null($maxLength) || ! (strlen($extraFilter) > $maxLength))) throw new Exception("[ extraFilter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraFilter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraFilter) === 1)) throw new Exception("[ extraFilter ] : ".$regex['message']);
		$this->extraFilter = $extraFilter;
		$this->addToUpdateList("extraFilter", $extraFilter);
		return $this;
	}
	public function getExtraFilter(){
		return $this->extraFilter;
	}
	public function setExtraInformation($extraInformation){
		$maxLength = self::getMaximumLength('extraInformation');
		if (! (is_null($maxLength) || ! (strlen($extraInformation) > $maxLength))) throw new Exception("[ extraInformation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraInformation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraInformation) === 1)) throw new Exception("[ extraInformation ] : ".$regex['message']);
		$this->extraInformation = $extraInformation;
		$this->addToUpdateList("extraInformation", $extraInformation);
		return $this;
	}
	public function getExtraInformation(){
		return $this->extraInformation;
	}
	public function setFlags($flags){
		$maxLength = self::getMaximumLength('flags');
		if (! (is_null($maxLength) || ! (strlen($flags) > $maxLength))) throw new Exception("[ flags ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('flags');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $flags) === 1)) throw new Exception("[ flags ] : ".$regex['message']);
		$this->flags = $flags;
		$this->addToUpdateList("flags", $flags);
		return $this;
	}
	public function getFlags(){
		return $this->flags;
	}
	public static function getId0Columnname()   { return "loginId"; }
	public static function getIdColumnnames() { return array("loginId"); }
	public static function getReferenceClass($pname)    {
		$tArray1 = array('group' => 'Group', 'sex' => 'Sex', 'marital' => 'Marital', 'jobTitle' => 'JobTitle', 'status' => 'UserStatus', 'firstSecurityQuestion' => 'SecurityQuestion', 'secondSecurityQuestion' => 'SecurityQuestion', 'firstDayOfAWeek' => 'DaysOfAWeek', 'lastLoginTime' => 'DateAndTime');
		$refclass = null; if (isset($tArray1[$pname])) $refclass = $tArray1[$pname];
		return $refclass;
	}
	public static function getColumnType($pname)    {
		$tArray1 = array('loginId' => 'integer', 'loginName' => 'text', 'password' => 'text', 'fullName' => 'text', 'group' => 'object', 'sex' => 'object', 'marital' => 'object', 'context' => 'text', 'jobTitle' => 'object', 'root' => 'boolean', 'initializationCounter' => 'integer', 'phone' => 'text', 'email' => 'text', 'status' => 'object', 'photo' => 'text', 'firstSecurityQuestion' => 'object', 'firstSecurityAnswer' => 'text', 'secondSecurityQuestion' => 'object', 'secondSecurityAnswer' => 'text', 'firstDayOfAWeek' => 'object', 'randomNumber' => 'text', 'lastLoginTime' => 'text', 'extraFilter' => 'text', 'extraInformation' => 'text', 'flags' => 'integer');
		$type = null; if (isset($tArray1[$pname])) $type = $tArray1[$pname];
		return $type;
	}
	public static function getRegularExpression($colname)   {
		$tArray1 = array();
		$tArray1['loginName'] = array();		$tArray1['loginName']['rule'] = "^([A-Za-z](\w){1,31})|(((\w+\+*\-*)+\.?)+@((\w+\+*\-*)+\.?)*[\w-]+\.[a-z]{2,6})$";		$tArray1['loginName']['message'] = "Username, min length=2, max length=32, must start with Alphabet and no special characters ie. ' ^ & etc or a VALID EMAIL ADDRESS";
		$tArray1['fullName'] = array();		$tArray1['fullName']['rule'] = "^[A-Za-z](\w|\s){1,47}$";		$tArray1['fullName']['message'] = "2 - 48 characters, must start with an Alphabet";
		$regexArray1 = null;
		if (isset($tArray1[$colname])) $regexArray1 = $tArray1[$colname];
		return $regexArray1;
	}
	public static function getMaximumLength($colname)    {
		$tArray1 = array();
		$tArray1['loginName'] = 56; 
		$tArray1['password'] = 40; 
		$tArray1['fullName'] = 48; 
		$tArray1['context'] = 255; 
		$tArray1['phone'] = 16; 
		$tArray1['email'] = 32; 
		$tArray1['photo'] = 64; 
		$tArray1['firstSecurityAnswer'] = 32; 
		$tArray1['secondSecurityAnswer'] = 32; 
		$tArray1['randomNumber'] = 32; 
		$tArray1['lastLoginTime'] = 19; 
		$tArray1['extraFilter'] = 32; 
		$tArray1['extraInformation'] = 64; 
		$length = null;
		if (isset($tArray1[$colname])) $length = $tArray1[$colname];
		return $length;
	}
	public function getMyClassname()    { return self::getClassname(); }
	public function getMyTablename()    { return self::getTablename(); }
	public function getMyId0Columnname()  { return self::getId0Columnname(); }
	public static function getClassname()  { return "Login"; }
	public static function getTablename()  { return "_login"; }
	public static function column2Property($colname)    {
		$tArray1 = array(
			"loginId" => "loginId"
			, "loginName" => "loginName"
			, "password" => "password"
			, "fullName" => "fullName"
			, "groupId" => "group"
			, "sexId" => "sex"
			, "maritalId" => "marital"
			, "context" => "context"
			, "jobId" => "jobTitle"
			, "root" => "root"
			, "initializationCounter" => "initializationCounter"
			, "phone" => "phone"
			, "email" => "email"
			, "statusId" => "status"
			, "photo" => "photo"
			, "qnSecurity1" => "firstSecurityQuestion"
			, "ansSecurity1" => "firstSecurityAnswer"
			, "qnSecurity2" => "secondSecurityQuestion"
			, "ansSecurity2" => "secondSecurityAnswer"
			, "firstDayOfAWeekId" => "firstDayOfAWeek"
			, "randomNumber" => "randomNumber"
			, "lastLoginTime" => "lastLoginTime"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$pname = null;
		if (isset($tArray1[$colname])) $pname = $tArray1[$colname];
		return $pname;
	}
	public static function property2Column($pname)    {
		$tArray1 = array(
			"loginId" => "loginId"
			, "loginName" => "loginName"
			, "password" => "password"
			, "fullName" => "fullName"
			, "group" => "groupId"
			, "sex" => "sexId"
			, "marital" => "maritalId"
			, "context" => "context"
			, "jobTitle" => "jobId"
			, "root" => "root"
			, "initializationCounter" => "initializationCounter"
			, "phone" => "phone"
			, "email" => "email"
			, "status" => "statusId"
			, "photo" => "photo"
			, "firstSecurityQuestion" => "qnSecurity1"
			, "firstSecurityAnswer" => "ansSecurity1"
			, "secondSecurityQuestion" => "qnSecurity2"
			, "secondSecurityAnswer" => "ansSecurity2"
			, "firstDayOfAWeek" => "firstDayOfAWeekId"
			, "randomNumber" => "randomNumber"
			, "lastLoginTime" => "lastLoginTime"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$colname = null;
		if (isset($tArray1[$pname])) $colname = $tArray1[$pname];
		return $colname;
	}
	public static function getColumnLookupTable()   {
		$tArray1 = array();
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "loginId";		$tArray1[$tsize]['pname'] = "loginId";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "loginName";		$tArray1[$tsize]['pname'] = "loginName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "password";		$tArray1[$tsize]['pname'] = "password";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "fullName";		$tArray1[$tsize]['pname'] = "fullName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "groupId";		$tArray1[$tsize]['pname'] = "group";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "sexId";		$tArray1[$tsize]['pname'] = "sex";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "maritalId";		$tArray1[$tsize]['pname'] = "marital";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "context";		$tArray1[$tsize]['pname'] = "context";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "jobId";		$tArray1[$tsize]['pname'] = "jobTitle";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "root";		$tArray1[$tsize]['pname'] = "root";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "initializationCounter";		$tArray1[$tsize]['pname'] = "initializationCounter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "phone";		$tArray1[$tsize]['pname'] = "phone";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "email";		$tArray1[$tsize]['pname'] = "email";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "statusId";		$tArray1[$tsize]['pname'] = "status";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "photo";		$tArray1[$tsize]['pname'] = "photo";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "qnSecurity1";		$tArray1[$tsize]['pname'] = "firstSecurityQuestion";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "ansSecurity1";		$tArray1[$tsize]['pname'] = "firstSecurityAnswer";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "qnSecurity2";		$tArray1[$tsize]['pname'] = "secondSecurityQuestion";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "ansSecurity2";		$tArray1[$tsize]['pname'] = "secondSecurityAnswer";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "firstDayOfAWeekId";		$tArray1[$tsize]['pname'] = "firstDayOfAWeek";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "randomNumber";		$tArray1[$tsize]['pname'] = "randomNumber";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "lastLoginTime";		$tArray1[$tsize]['pname'] = "lastLoginTime";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraFilter";		$tArray1[$tsize]['pname'] = "extraFilter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraInformation";		$tArray1[$tsize]['pname'] = "extraInformation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "flags";		$tArray1[$tsize]['pname'] = "flags";
		return $tArray1;
	}
	public static function columnTransitiveMap($pname)  {
		$tArray1 =  array('loginId' => 'loginId', 'loginName' => 'loginName', 'password' => 'password', 'fullName' => 'fullName', 'group' => array('Group.groupName'), 'sex' => array('Sex.sexName'), 'marital' => array('Marital.maritalName'), 'context' => 'context', 'jobTitle' => array('JobTitle.jobName'), 'root' => 'root', 'initializationCounter' => 'initializationCounter', 'phone' => 'phone', 'email' => 'email', 'status' => array('UserStatus.statusName'), 'photo' => 'photo', 'firstSecurityQuestion' => array('SecurityQuestion.questionName'), 'firstSecurityAnswer' => 'firstSecurityAnswer', 'secondSecurityQuestion' => array('SecurityQuestion.questionName'), 'secondSecurityAnswer' => 'secondSecurityAnswer', 'firstDayOfAWeek' => array('DaysOfAWeek.dayName'), 'randomNumber' => 'randomNumber', 'lastLoginTime' => 'lastLoginTime', 'extraFilter' => 'extraFilter', 'extraInformation' => 'extraInformation', 'flags' => 'flags');
		$pmap = null; if (isset($tArray1[$pname])) $pmap = $tArray1[$pname];
		return $pmap;
	}
	public static function getSearchableColumns()    {
		/* Will return list of Searchable Properties */
		return array('loginName', 'fullName', 'group', 'sex', 'marital', 'context', 'jobTitle', 'root', 'initializationCounter', 'phone', 'email', 'status', 'photo', 'firstSecurityQuestion', 'firstSecurityAnswer', 'secondSecurityQuestion', 'secondSecurityAnswer', 'firstDayOfAWeek', 'randomNumber', 'lastLoginTime', 'extraFilter', 'extraInformation', 'flags');
	}
	public static function getASearchUI($page, $listOfColumnsToDisplay, $optIndex = 0)    {
		$line = "";
		$mycolumnlist = json_encode($listOfColumnsToDisplay);
		$line .= "&lt;div class=&quot;container __ui_search_container__ py-2&quot;&gt;    &lt;div class=&quot;row&quot;&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;    &lt;form id=&quot;__delta_init_basic__&quot;&gt;        &lt;div class=&quot;input-group mb-3&quot;&gt;            &lt;input name=&quot;__ui_search_input__&quot; id=&quot;__ui_search_input__$optIndex&quot; type=&quot;search&quot; data-min-length=&quot;3&quot;                class=&quot;form-control&quot;required placeholder=&quot;Search&quot; aria-label=&quot;Search&quot; aria-describedby=&quot;basic-addon2&quot; /&gt;            &lt;div class=&quot;input-group-append&quot;&gt;                &lt;button id=&quot;__ui_search_button__$optIndex&quot; data-form-id=&quot;__delta_init_basic__&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot;                    data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot;                    data-column='[&quot;loginName&quot;]' data-page='$page' data-class='Login'                    class=&quot;btn btn-outline-primary btn-perform-search&quot; type=&quot;button&quot;      data-search-input=&quot;text&quot; data-search-input-id=&quot;__ui_search_input__$optIndex&quot; data-toggle=&quot;tooltip&quot;                    title=&quot;This is a basic search&quot;&gt;Search&lt;/button&gt;            &lt;/div&gt;        &lt;/div&gt;    &lt;/form&gt;&lt;/div&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;&lt;button type=&quot;button &quot;class=&quot;btn btn-outline-primary btn-block&quot; name=&quot;__ui_advanced_search_button__&quot; id=&quot;__ui_advanced_search_button__$optIndex&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot; data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot; data-column='[&quot;loginId&quot;,&quot;loginName&quot;,&quot;fullName&quot;,&quot;group&quot;,&quot;sex&quot;,&quot;marital&quot;,&quot;context&quot;,&quot;jobTitle&quot;,&quot;root&quot;,&quot;initializationCounter&quot;,&quot;phone&quot;,&quot;email&quot;,&quot;status&quot;,&quot;photo&quot;,&quot;firstSecurityQuestion&quot;,&quot;firstSecurityAnswer&quot;,&quot;secondSecurityQuestion&quot;,&quot;secondSecurityAnswer&quot;,&quot;firstDayOfAWeek&quot;,&quot;randomNumber&quot;,&quot;lastLoginTime&quot;,&quot;extraFilter&quot;,&quot;extraInformation&quot;,&quot;flags&quot;]' data-min-length=&quot;3&quot; data-page='$page' data-class='Login' data-search-dialog=&quot;__dialog_search_container_01__&quot; data-toggle=&quot;tooltip&quot; title=&quot;This is a more advanced search technique&quot;&gt;Advanced Search&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;&lt;br/&gt;&lt;div id=&quot;__ui_search_error__$optIndex&quot; class=&quot;p-1 ui-sys-error-message&quot;&gt;&lt;/div&gt;&lt;div style=&quot;overflow-x: scroll;&quot; id=&quot;__ui_search_output_target__$optIndex&quot;&gt;&lt;/div&gt;&lt;/div&gt;";
		$line .= "&lt;script type=&quot;text/javascript&quot;&gt;(function($)    {    $(function()    {        var callbackFunction$optIndex = function(\$button1, data, textStatus, optionArgumentArray1) {            var \$dialog1 = $('#' + \$button1.data('searchDialog'));            \$dialog1 = showAdvancedSearchDialog(\$button1, \$dialog1, data, Constant);            \$dialog1.modal('show');      };        $('#__ui_advanced_search_button__$optIndex').on('click', function(e)   {            var \$button1 = $(this);            var columnList = \$button1.data('column');            var classname = \$button1.data('class');            var payload = { columns: columnList, classname: classname };            fSendAjax(\$button1,                    $('&lt;span/&gt;'),                    '../server/serviceGetAdvancedSearchPayload.php',                    payload,                    null,                    null,                    callbackFunction$optIndex,                    null,                    null,                    &quot;POST&quot;,                    true,                    false,                    &quot;Processing ....&quot;,                    null,                    null);        });    });})(jQuery);&lt;/script&gt;";
		return htmlspecialchars_decode($line);
	}
	public function getMyPropertyValue($pname)  {
		if ($pname == "loginId") return $this->loginId;
		else if ($pname == "loginName") return $this->loginName;
		else if ($pname == "password") return $this->password;
		else if ($pname == "fullName") return $this->fullName;
		else if ($pname == "group") return $this->group;
		else if ($pname == "sex") return $this->sex;
		else if ($pname == "marital") return $this->marital;
		else if ($pname == "context") return $this->context;
		else if ($pname == "jobTitle") return $this->jobTitle;
		else if ($pname == "root") return $this->root;
		else if ($pname == "initializationCounter") return $this->initializationCounter;
		else if ($pname == "phone") return $this->phone;
		else if ($pname == "email") return $this->email;
		else if ($pname == "status") return $this->status;
		else if ($pname == "photo") return $this->photo;
		else if ($pname == "firstSecurityQuestion") return $this->firstSecurityQuestion;
		else if ($pname == "firstSecurityAnswer") return $this->firstSecurityAnswer;
		else if ($pname == "secondSecurityQuestion") return $this->secondSecurityQuestion;
		else if ($pname == "secondSecurityAnswer") return $this->secondSecurityAnswer;
		else if ($pname == "firstDayOfAWeek") return $this->firstDayOfAWeek;
		else if ($pname == "randomNumber") return $this->randomNumber;
		else if ($pname == "lastLoginTime") return $this->lastLoginTime;
		else if ($pname == "extraFilter") return $this->extraFilter;
		else if ($pname == "extraInformation") return $this->extraInformation;
		else if ($pname == "flags") return $this->flags;
		return null;
	}
	public static function getValue0Columnname() {
		return "loginName";
	}
	public static function getValueColumnnames()   {
		return array('loginName');
	}
	public function getName() {
		return array($this->loginName);
	}
	public function getName0() {
		$namedValue = $this->loginName;
		return $namedValue;
	}
}
?>