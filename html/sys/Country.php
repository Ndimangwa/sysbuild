<?php
/******************************************************
**                                                   **
**                CLASSNAME : Country                **
**  Copyright (c) Zoomtong Company Limited           **
**  Developed by : Ndimangwa Fadhili Ngoya           **
**  Timestamp    : 2021:08:01:21:14:04               **
**  Phones       : +255 787 101 808 / 762 357 596    **
**  Email        : ndimangwa@gmail.com               **
**  Address      : P.O BOX 7436 MOSHI, TANZANIA      **
**                                                   **
**  Dedication to my dear wife Valentina             **
**                my daughters Raheli & Keziah       **
**                                                   **
*******************************************************/
class Country extends __data__ {
	protected $database;
	protected $conn;
	private $countryId;
	private $countryName;
	private $abbreviation;
	private $code;
	private $timezone;
	private $extraFilter;
	private $extraInformation;
	private $flags;
/*BEGIN OF CUSTOM CODES : You should Add Your Custom Codes Below this line*/

/*END OF CUSTOM CODES : You should Add Your Custom Codes Above this line*/
	public static function create($database, $id, $conn) { return new Country($database, $id, $conn); }
	public function __construct($database, $id, $conn)    {
		$this->setMe($database, $id, $conn);
	}
	public function setMe($database, $id, $conn)    {
		$this->database = $database;
		$this->conn = $conn;
		$whereClause = self::getId0Columnname();
		$whereClause = array($whereClause => $id);
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), array('*'), $whereClause);
		$jresult1 = SQLEngine::execute($query, $conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] !== 1) throw new Exception("Duplicate or no record found");
		$resultSet = $jArray1['rows'][0];
		if (! array_key_exists("countryId", $resultSet)) throw new Exception("Column [countryId] not available while pulling data");
		$this->countryId = $resultSet["countryId"];
		if (! array_key_exists("countryName", $resultSet)) throw new Exception("Column [countryName] not available while pulling data");
		$this->setCountryName($resultSet["countryName"]);
		if (! array_key_exists("abbreviation", $resultSet)) throw new Exception("Column [abbreviation] not available while pulling data");
		$this->setAbbreviation($resultSet["abbreviation"]);
		if (! array_key_exists("code", $resultSet)) throw new Exception("Column [code] not available while pulling data");
		$this->setCode($resultSet["code"]);
		if (! array_key_exists("timezone", $resultSet)) throw new Exception("Column [timezone] not available while pulling data");
		$this->setTimezone($resultSet["timezone"]);
		if (! array_key_exists("extraFilter", $resultSet)) throw new Exception("Column [extraFilter] not available while pulling data");
		$this->setExtraFilter($resultSet["extraFilter"]);
		if (! array_key_exists("extraInformation", $resultSet)) throw new Exception("Column [extraInformation] not available while pulling data");
		$this->setExtraInformation($resultSet["extraInformation"]);
		if (! array_key_exists("flags", $resultSet)) throw new Exception("Column [flags] not available while pulling data");
		$this->setFlags($resultSet["flags"]);
		$this->clearUpdateList();
		return $this;
	}
	public static function loadAllData($__conn) {
		$colArray1 = array('countryId', 'countryName');
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), $colArray1, null);
		$jresult1 = SQLEngine::execute($query, $__conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		$dataArray1 = array();
		foreach ($jArray1['rows'] as $resultSet)    {
			$index = sizeof($dataArray1); $dataArray1[$index] = array();
			$dataArray1[$index]['__id__'] = $resultSet['countryId'];
			$myval = "";
			$myval .= " ".$resultSet['countryName'];
			$dataArray1[$index]['__name__'] = trim($myval);
		}
		return $dataArray1;
	}
	public function getId() { return md5($this->countryId); }
	public function getIdWhereClause() { return "{ \"countryId\" : $this->countryId }"; }
	public function getId0()  { return $this->countryId; }
	public function getId0WhereClause()  { return "{ \"countryId\" : $this->countryId }"; }
	public function getCountryId(){
		return $this->countryId;
	}
	public function setCountryName($countryName){
		$maxLength = self::getMaximumLength('countryName');
		if (! (is_null($maxLength) || ! (strlen($countryName) > $maxLength))) throw new Exception("[ countryName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('countryName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $countryName) === 1)) throw new Exception("[ countryName ] : ".$regex['message']);
		$this->countryName = $countryName;
		$this->addToUpdateList("countryName", $countryName);
		return $this;
	}
	public function getCountryName(){
		return $this->countryName;
	}
	public function setAbbreviation($abbreviation){
		$maxLength = self::getMaximumLength('abbreviation');
		if (! (is_null($maxLength) || ! (strlen($abbreviation) > $maxLength))) throw new Exception("[ abbreviation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('abbreviation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $abbreviation) === 1)) throw new Exception("[ abbreviation ] : ".$regex['message']);
		$this->abbreviation = $abbreviation;
		$this->addToUpdateList("abbreviation", $abbreviation);
		return $this;
	}
	public function getAbbreviation(){
		return $this->abbreviation;
	}
	public function setCode($code){
		$maxLength = self::getMaximumLength('code');
		if (! (is_null($maxLength) || ! (strlen($code) > $maxLength))) throw new Exception("[ code ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('code');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $code) === 1)) throw new Exception("[ code ] : ".$regex['message']);
		$this->code = $code;
		$this->addToUpdateList("code", $code);
		return $this;
	}
	public function getCode(){
		return $this->code;
	}
	public function setTimezone($timezone){
		$maxLength = self::getMaximumLength('timezone');
		if (! (is_null($maxLength) || ! (strlen($timezone) > $maxLength))) throw new Exception("[ timezone ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('timezone');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $timezone) === 1)) throw new Exception("[ timezone ] : ".$regex['message']);
		$this->timezone = $timezone;
		$this->addToUpdateList("timezone", $timezone);
		return $this;
	}
	public function getTimezone(){
		return $this->timezone;
	}
	public function setExtraFilter($extraFilter){
		$maxLength = self::getMaximumLength('extraFilter');
		if (! (is_null($maxLength) || ! (strlen($extraFilter) > $maxLength))) throw new Exception("[ extraFilter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraFilter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraFilter) === 1)) throw new Exception("[ extraFilter ] : ".$regex['message']);
		$this->extraFilter = $extraFilter;
		$this->addToUpdateList("extraFilter", $extraFilter);
		return $this;
	}
	public function getExtraFilter(){
		return $this->extraFilter;
	}
	public function setExtraInformation($extraInformation){
		$maxLength = self::getMaximumLength('extraInformation');
		if (! (is_null($maxLength) || ! (strlen($extraInformation) > $maxLength))) throw new Exception("[ extraInformation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraInformation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraInformation) === 1)) throw new Exception("[ extraInformation ] : ".$regex['message']);
		$this->extraInformation = $extraInformation;
		$this->addToUpdateList("extraInformation", $extraInformation);
		return $this;
	}
	public function getExtraInformation(){
		return $this->extraInformation;
	}
	public function setFlags($flags){
		$maxLength = self::getMaximumLength('flags');
		if (! (is_null($maxLength) || ! (strlen($flags) > $maxLength))) throw new Exception("[ flags ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('flags');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $flags) === 1)) throw new Exception("[ flags ] : ".$regex['message']);
		$this->flags = $flags;
		$this->addToUpdateList("flags", $flags);
		return $this;
	}
	public function getFlags(){
		return $this->flags;
	}
	public static function getId0Columnname()   { return "countryId"; }
	public static function getIdColumnnames() { return array("countryId"); }
	public static function getReferenceClass($pname)    {
		$tArray1 = array();
		$refclass = null; if (isset($tArray1[$pname])) $refclass = $tArray1[$pname];
		return $refclass;
	}
	public static function getColumnType($pname)    {
		$tArray1 = array('countryId' => 'integer', 'countryName' => 'text', 'abbreviation' => 'text', 'code' => 'integer', 'timezone' => 'text', 'extraFilter' => 'text', 'extraInformation' => 'text', 'flags' => 'integer');
		$type = null; if (isset($tArray1[$pname])) $type = $tArray1[$pname];
		return $type;
	}
	public static function getRegularExpression($colname)   {
		$tArray1 = array();
		$regexArray1 = null;
		if (isset($tArray1[$colname])) $regexArray1 = $tArray1[$colname];
		return $regexArray1;
	}
	public static function getMaximumLength($colname)    {
		$tArray1 = array();
		$tArray1['countryName'] = 48; 
		$tArray1['abbreviation'] = 4; 
		$tArray1['timezone'] = 5; 
		$tArray1['extraFilter'] = 32; 
		$tArray1['extraInformation'] = 64; 
		$length = null;
		if (isset($tArray1[$colname])) $length = $tArray1[$colname];
		return $length;
	}
	public function getMyClassname()    { return self::getClassname(); }
	public function getMyTablename()    { return self::getTablename(); }
	public function getMyId0Columnname()  { return self::getId0Columnname(); }
	public static function getClassname()  { return "Country"; }
	public static function getTablename()  { return "_country"; }
	public static function column2Property($colname)    {
		$tArray1 = array(
			"countryId" => "countryId"
			, "countryName" => "countryName"
			, "abbreviation" => "abbreviation"
			, "code" => "code"
			, "timezone" => "timezone"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$pname = null;
		if (isset($tArray1[$colname])) $pname = $tArray1[$colname];
		return $pname;
	}
	public static function property2Column($pname)    {
		$tArray1 = array(
			"countryId" => "countryId"
			, "countryName" => "countryName"
			, "abbreviation" => "abbreviation"
			, "code" => "code"
			, "timezone" => "timezone"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$colname = null;
		if (isset($tArray1[$pname])) $colname = $tArray1[$pname];
		return $colname;
	}
	public static function getColumnLookupTable()   {
		$tArray1 = array();
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "countryId";		$tArray1[$tsize]['pname'] = "countryId";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "countryName";		$tArray1[$tsize]['pname'] = "countryName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "abbreviation";		$tArray1[$tsize]['pname'] = "abbreviation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "code";		$tArray1[$tsize]['pname'] = "code";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "timezone";		$tArray1[$tsize]['pname'] = "timezone";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraFilter";		$tArray1[$tsize]['pname'] = "extraFilter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraInformation";		$tArray1[$tsize]['pname'] = "extraInformation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "flags";		$tArray1[$tsize]['pname'] = "flags";
		return $tArray1;
	}
	public static function columnTransitiveMap($pname)  {
		$tArray1 =  array('countryId' => 'countryId', 'countryName' => 'countryName', 'abbreviation' => 'abbreviation', 'code' => 'code', 'timezone' => 'timezone', 'extraFilter' => 'extraFilter', 'extraInformation' => 'extraInformation', 'flags' => 'flags');
		$pmap = null; if (isset($tArray1[$pname])) $pmap = $tArray1[$pname];
		return $pmap;
	}
	public static function getSearchableColumns()    {
		/* Will return list of Searchable Properties */
		return array('countryName', 'abbreviation', 'code', 'timezone', 'extraFilter', 'extraInformation', 'flags');
	}
	public static function getASearchUI($page, $listOfColumnsToDisplay, $optIndex = 0)    {
		$line = "";
		$mycolumnlist = json_encode($listOfColumnsToDisplay);
		$line .= "&lt;div class=&quot;container __ui_search_container__ py-2&quot;&gt;    &lt;div class=&quot;row&quot;&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;    &lt;form id=&quot;__delta_init_basic__&quot;&gt;        &lt;div class=&quot;input-group mb-3&quot;&gt;            &lt;input name=&quot;__ui_search_input__&quot; id=&quot;__ui_search_input__$optIndex&quot; type=&quot;search&quot; data-min-length=&quot;3&quot;                class=&quot;form-control&quot;required placeholder=&quot;Search&quot; aria-label=&quot;Search&quot; aria-describedby=&quot;basic-addon2&quot; /&gt;            &lt;div class=&quot;input-group-append&quot;&gt;                &lt;button id=&quot;__ui_search_button__$optIndex&quot; data-form-id=&quot;__delta_init_basic__&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot;                    data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot;                    data-column='[&quot;countryName&quot;]' data-page='$page' data-class='Country'                    class=&quot;btn btn-outline-primary btn-perform-search&quot; type=&quot;button&quot;      data-search-input=&quot;text&quot; data-search-input-id=&quot;__ui_search_input__$optIndex&quot; data-toggle=&quot;tooltip&quot;                    title=&quot;This is a basic search&quot;&gt;Search&lt;/button&gt;            &lt;/div&gt;        &lt;/div&gt;    &lt;/form&gt;&lt;/div&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;&lt;button type=&quot;button &quot;class=&quot;btn btn-outline-primary btn-block&quot; name=&quot;__ui_advanced_search_button__&quot; id=&quot;__ui_advanced_search_button__$optIndex&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot; data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot; data-column='[&quot;countryId&quot;,&quot;countryName&quot;,&quot;abbreviation&quot;,&quot;code&quot;,&quot;timezone&quot;,&quot;extraFilter&quot;,&quot;extraInformation&quot;,&quot;flags&quot;]' data-min-length=&quot;3&quot; data-page='$page' data-class='Country' data-search-dialog=&quot;__dialog_search_container_01__&quot; data-toggle=&quot;tooltip&quot; title=&quot;This is a more advanced search technique&quot;&gt;Advanced Search&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;&lt;br/&gt;&lt;div id=&quot;__ui_search_error__$optIndex&quot; class=&quot;p-1 ui-sys-error-message&quot;&gt;&lt;/div&gt;&lt;div style=&quot;overflow-x: scroll;&quot; id=&quot;__ui_search_output_target__$optIndex&quot;&gt;&lt;/div&gt;&lt;/div&gt;";
		$line .= "&lt;script type=&quot;text/javascript&quot;&gt;(function($)    {    $(function()    {        var callbackFunction$optIndex = function(\$button1, data, textStatus, optionArgumentArray1) {            var \$dialog1 = $('#' + \$button1.data('searchDialog'));            \$dialog1 = showAdvancedSearchDialog(\$button1, \$dialog1, data, Constant);            \$dialog1.modal('show');      };        $('#__ui_advanced_search_button__$optIndex').on('click', function(e)   {            var \$button1 = $(this);            var columnList = \$button1.data('column');            var classname = \$button1.data('class');            var payload = { columns: columnList, classname: classname };            fSendAjax(\$button1,                    $('&lt;span/&gt;'),                    '../server/serviceGetAdvancedSearchPayload.php',                    payload,                    null,                    null,                    callbackFunction$optIndex,                    null,                    null,                    &quot;POST&quot;,                    true,                    false,                    &quot;Processing ....&quot;,                    null,                    null);        });    });})(jQuery);&lt;/script&gt;";
		return htmlspecialchars_decode($line);
	}
	public function getMyPropertyValue($pname)  {
		if ($pname == "countryId") return $this->countryId;
		else if ($pname == "countryName") return $this->countryName;
		else if ($pname == "abbreviation") return $this->abbreviation;
		else if ($pname == "code") return $this->code;
		else if ($pname == "timezone") return $this->timezone;
		else if ($pname == "extraFilter") return $this->extraFilter;
		else if ($pname == "extraInformation") return $this->extraInformation;
		else if ($pname == "flags") return $this->flags;
		return null;
	}
	public static function getValue0Columnname() {
		return "countryName";
	}
	public static function getValueColumnnames()   {
		return array('countryName');
	}
	public function getName() {
		return array($this->countryName);
	}
	public function getName0() {
		$namedValue = $this->countryName;
		return $namedValue;
	}
}
?>