<?php 
    /*
Developed by Ndimangwa Fadhili Ngoya
Developed on 15th April, 2021
Phone: +255 787 101 808 / +255 762 357 596
Email: ndimangwa@gmail.com 

Initialization codes for data related objects
*/
abstract class __data__ extends __object__ {
	public static $__PROFILE_INIT_ID = 1;
	public static $__LOGIN_INIT_ID = 1;
	public static $__USER_INIT_ID = 1;
	private $l__update = array(); //used to keep track of updates
	public static $__ALL32BITS_SET = 4294967295;
	//Working with Data
	public static function convertRawSQLDataToTabularData($conn, $classname, $rows)	{
		$tArray1 = array();
		$primaryColumn = Registry::getId0Columnname($classname);
		foreach ($rows as $row)	{
			$index = sizeof($tArray1);
			$tArray1[$index] = array();
			foreach ($row as $colname => $value)	{
				$pname = Registry::column2Property($classname, $colname);
				if (is_null($pname)) continue;
				if ($colname == $primaryColumn) $pname = "id";
				//We need to work for value 
				$tmap = Registry::columnTransitiveMap($classname, $pname);
				$newValue = null;
				if (! is_null($tmap) && is_array($tmap))	{
					foreach ($tmap as $tclassproperty)	{
						$tval = self::getValueOfAClassProperty($conn, $tclassproperty, $value);
						if (! is_null($tval))	{
							if (is_null($newValue)) $newValue = $tval;
						} else {
							$newValue .= " -- $tval";
						}
					}
				}
				if (is_null($newValue)) $newValue = $value;
				$tArray1[$index][$pname] = $newValue;
			}
		}
		return $tArray1;
	}
	public static function getValueOfAClassProperty($conn, $classproperty /* ie Sex.sexName */, $pid /*  ie 2*/)	{
		// return value of a property of a class given a primary value 
		if (is_null($classproperty)) return null;
		$tArray1 = explode(".", $classproperty);
		if (sizeof($tArray1) != 2) return null;
		$classname = $tArray1[0];
		$pname = $tArray1[1];
		$primaryColumn = Registry::getId0Columnname($classname);
		$colname = Registry::property2column($classname, $pname);
		if (is_null($primaryColumn)) return null;
		$jresult1 = SQLEngine::execute(SimpleQueryBuilder::buildSelect(
			array(Registry::getTablename($classname)),
			array($colname),
			array($primaryColumn => $pid)
		), $conn);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) return null;
		if ($jArray1['code'] !== 0) return null;
		if ($jArray1['count'] !== 1) return null; //Must be one , since we submited pid -- unique 
		return $jArray1['rows'][0][$colname];
	}
	//Alerts And Warnings 
	private static function getAlert($message, $alertType)	{
		return "<div class=\"alert $alertType\" role=\"alert\">$message</div>";
	}
	public static function showDangerAlert($message)	{
		return self::getAlert($message, "alert-danger");
	}
	public static function showWarningAlert($message)	{
		return self::getAlert($message, "alert-warning");
	}
	//FormControls 
	public static function createDetailsPage($page, $classname, $payload /* 1D columnlist */, $conn, $id, $extraControls = null)	{
		$line = "<div class=\"data_details\"><table class=\"table\"><thead class=\"thead-dark\"><th scope=\"col\"></th><th>Name</th><th>Value</th></thead><tbody>";
		$tablename = Registry::getTablename($classname);
		$primaryColumn = Registry::getId0Columnname($classname);
		$whereArray1 = array();
		$whereArray1[$primaryColumn] = $id;
		$listOfColumns = array();
		foreach ($payload as $pname)	{
			$dt = Registry::property2Column($classname, $pname);
			if (! is_null($dt))	{
				$listOfColumns[sizeof($listOfColumns)] = $dt;
			}
		}
		$jresult1 = SQLEngine::execute(SimpleQueryBuilder::buildSelect(
			array($tablename),
			$listOfColumns,
			$whereArray1
		), $conn);
		if (is_null($jresult1)) throw new Exception("Results Returned null");
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("Malformed Return Value");
		if ($jArray1['code'] != 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] != 1) throw new Exception("Empty, or Duplicate records");
		$rows = self::convertRawSQLDataToTabularData($conn, $classname, $jArray1['rows']);
		$row = $rows[0];
		$count = 0;
		foreach ($listOfColumns as $column)	{
			$pname = Registry::column2property($classname, $column);
			if (! is_null($pname))	{
				$sn = $count + 1;
				$caption = __object__::property2Caption($pname);
				$value = $row[$pname];
				if (is_null($value)) continue;
				if ($value == "") continue;
				//We need to do translation
				$line .= "<tr><th scope=\"row\">$sn</th><td>$caption</td><td>$value</td></tr>";
				$count++;
			}
		}
		$line .= "</tbody></table>";
		//Working with further controls 
		if (! is_null($extraControls))	{
			$line .= "<div class=\"extra-controls-container\" class=\"mt-2 mr-2 mb-2 ml-auto text-right\">";
			$count = 0;
			foreach ($extraControls as $controlBlock1)	{
				if (! (isset($controlBlock1['pname']) && isset($controlBlock1['href']))) continue;
				$pname = $controlBlock1['pname'];
				$href = $controlBlock1['href'];
				$caption = null; if (isset($controlBlock1['caption'])) $caption = $controlBlock1['caption'];
				$policy = null; if (isset($controlBlock1['policy'])) $policy = $controlBlock1['policy'];
				$title = null; if (isset($controlBlock1['title']))	$title = $controlBlock1['title'];
				$icon = null; if (isset($controlBlock1['icon-class'])) $icon = $controlBlock1['icon-class'];
				if (is_null($policy) || $policy)	{
					
					$m_left = "ml-2"; if ($count == 0) $m_left = "";
					if (! is_null($title)) $title = "title = \"$title\""; else $title = "";
					if (is_null($caption)) $caption = ""; 
					if (! is_null($icon)) $icon = "<i class=\"$icon\"></i>";else $icon = "";
					$line .= "<a class=\"cmd cmd_$pname $m_left\" href=\"$href\" data-id=\"$id\" data-class=\"$classname\" data-toggle=\"tooltip\" $title>$icon $caption</a>";
					$count++;
				}
			}
			$line .= "</div>";
		}
		$line .= "</div>";
		return $line;
	}
	public static function createConfirmationForm($page, $classname, $message, $mode = "delete", $conn = null, $id = -1, $customHiddenFields = null, $customContextName = null)	{
		$formid = __object__::getCodeString(16);
		$errorid = "__form_error_$formid";
		$formid = "__form_reference_$formid";
		$line = "<form method=\"POST\" id=\"$formid\">";
		$line .= "<input type=\"hidden\" name=\"__classname__\" value=\"$classname\"/>";
		if (! is_null($customContextName)) $line .= "<input type=\"hidden\" name=\"__custom_context_name__\" value=\"$customContextName\"/>";
		$line .= "<input type=\"hidden\" name=\"__query__\" value=\"$mode\"/>";
		if (! is_null($customHiddenFields))	{
			foreach ($customHiddenFields as $keyname => $val)	{
				$line .= "<input type=\"hidden\" name=\"$keyname\" value=\"$val\"/>";
			}
		}
		$line .= "<div><label>$message</label></div>";
		//Ui Error Field
		$line .= "<div id=\"$errorid\" class=\"p-2 ui-sys-error-message\"></div>";
		//UI Submit Buttons
		$line .= "<div><button class=\"btn btn-danger btn-block btn-click-default btn-execute-on-click btn-send-dialog-ajax\" type=\"button\" data-form-submit=\"$formid\" data-next-page=\"$page\" data-form-error=\"$errorid\">Submit</button></div>";
		$line .= "</form>";
		return $line;
	}
	public static function createDataCaptureForm($page, $classname, $payload /*2D array*/, $mode = "create", $conn = null, $id = -1, $customHiddenFields = null /*key value*/, $customContextName = null)	{
		$formid = __object__::getCodeString(16);
		$errorid = "__form_error_$formid";
		$formid = "__form_reference_$formid";
		$line = "<form method=\"POST\" id=\"$formid\">";
		//Hidden Fields
		$line .= "<input type=\"hidden\" name=\"__classname__\" value=\"$classname\"/>";
		if (! is_null($customContextName)) $line .= "<input type=\"hidden\" name=\"__custom_context_name__\" value=\"$customContextName\"/>";
		$line .= "<input type=\"hidden\" name=\"__query__\" value=\"$mode\"/>";
		if (! is_null($customHiddenFields))	{
			foreach ($customHiddenFields as $keyname => $val)	{
				$line .= "<input type=\"hidden\" name=\"$keyname\" value=\"$val\"/>";
			}
		}
		//Ui Control Fields
		$object1 = null;
		if ($mode == "update") $object1 = Registry::getObjectReference("Ndimangwa-Ngoya", $conn, $classname, $id);
		foreach ($payload as $fieldBlock1)	{
			if (! isset($fieldBlock1['pname'])) continue;
			$pname = $fieldBlock1['pname'];
			$caption = null;
			if (! isset($fieldBlock1['caption'])) $caption = $fieldBlock1['caption'];
			else $caption = __object__::property2Caption($pname);
			$required = true; if (isset($fieldBlock1['required'])) $required = $fieldBlock1['required'];
			$placeholder = null; if (isset($fieldBlock1['placeholder'])) $placeholder = $fieldBlock1['placeholder'];
			$validationColumnName = null; if (isset($fieldBlock1['validation-column-name'])) $validationColumnName = $fieldBlock1['validation-column-name'];
			$uiRenderControl = null; if (isset($fieldBlock1['ui-control'])) $uiRenderControl = $fieldBlock1['ui-control'];
			//We need to Get Control Type 
			$type = Registry::getColumnType($classname, $pname);
			if (isset($fieldBlock1['type'])) $type = $fieldBlock1['type'];
			if (is_null($type)) continue;
			$propertyValue = null; if (! is_null($object1)) $propertyValue = $object1->getMyPropertyValue($pname);
			if ($type == "object")	{
				$line .= self::createFormSelectInput($conn, $classname, $pname, $caption, $propertyValue, $required, $validationColumnName);
			} else if ($type == "text")	{
				$line .= self::createFormTextInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder);
			} else if ($type == "integer")	{
				$line .= self::createFormNumberInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder);
			} else if ($type == "email")	{
				$line .= self::createFormEmailInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder);
			}
			//echo "\n[ type = $type ] ; ==== [ pname = $pname ]";
		}
		//Ui Error Field
		$line .= "<div id=\"$errorid\" class=\"p-2 ui-sys-error-message\"></div>";
		//UI Submit Buttons
		$line .= "<div><button class=\"btn btn-primary btn-block btn-click-default btn-execute-on-click btn-send-dialog-ajax\" type=\"button\" data-form-submit=\"$formid\" data-next-page=\"$page\" data-form-error=\"$errorid\">Submit</button></div>";
		$line .= "</form>";
		return $line;
	}
	public static function createFormSelectInput($conn1, $classname, $name, $caption, $refObject1 = null, $isrequired = true, $validationColumnName = null)	{
		$required = ""; if ($isrequired) $required = "required";
		$validationRule = Registry::getUIControlValidations($classname, $name, "select"); //All Inputs are validated as text , unless otherwise, select control not included
		$defaultValue = Constant::$default_select_empty_value;
		$control1 =  "<div class=\"form-group\"><label for=\"$name\">$caption</label><select class=\"form-control\" id=\"$name\" name=\"$name\" $required $validationRule><option value=\"$defaultValue\">(-- Select --)</option>";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$refClassname = Registry::getReferenceClass($classname, $validationColumnName);
		$dataArray1 = null; if (! is_null($refClassname)) $dataArray1 = Registry::loadAllData($conn1, $refClassname);
		if (! is_null($dataArray1))	{
			foreach ($dataArray1 as $dataBlock1)	{
				$dvalue = $dataBlock1['__id__'];
				$dcaption = $dataBlock1['__name__'];
				$selected = "";  if (! is_null($refObject1) && ($dvalue == $refObject1->getId0())) $selected = "selected";
				$control1 .= "<option $selected value=\"$dvalue\">$dcaption</option>";
			}
		}
		$control1 .= "</select></div>";
		return $control1;
	}
	public static function createFormPasswordInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "password", $validationColumnName, $placeholder);
	}
	public static function createFormNumberInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "number", $validationColumnName, $placeholder);
	}
	public static function createFormEmailInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "email", $validationColumnName, $placeholder);
	}
	public static function createFormTextInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "text", $validationColumnName, $placeholder);
	}
	public static function createGeneralFormInput($classname, $name, $caption, $value =null, $isrequired = true, $type = "text", $validationColumnName = null, $placeholder = null)	{
		$required = ""; if ($isrequired) $required = "required";
		$fvalue = ""; if (! is_null($value)) $fvalue = "value = \"$value\"";
		$fplaceholder = ""; if (! is_null($placeholder)) $fplaceholder = "placeholder = \"$placeholder\"";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$validationRule = Registry::getUIControlValidations($classname, $validationColumnName, "text"); //All Inputs are validated as text , unless otherwise, select control not included
		return "<div class=\"form-group\"><label for=\"$name\">$caption</label><input class=\"form-control\" id=\"$name\" name=\"$name\" type=\"$type\" $fvalue $required $fplaceholder $validationRule /></div>";
	}
    //01.Flags Management 
    public function setFlagAt($pos)	{
		$powerOfTwo = __object__::getPowerOfTwo($pos);
		if ($powerOfTwo != 0)	{
			$flagValue = intval("".$this->getFlags());
			$flagValue = $flagValue | $powerOfTwo;
			$this->setFlags($flagValue);
		}
	}
	public function resetFlagAt($pos)	{
		$powerOfTwo = __object__::getPowerOfTwo($pos);
		if ($powerOfTwo != 0)	{
			$flagValue = intval("".$this->getFlags());
			$powerOfTwo = self::$__ALL32BITS_SET - $powerOfTwo; //This is a 32bits operation 
			$flagValue = $flagValue & $powerOfTwo;
			$this->setFlags($flagValue);
		}
	}
	public function isFlagSetAt($pos)	{
		$powerOfTwo = __object__::getPowerOfTwo($pos);
		$blnSet = false;
		if ($powerOfTwo != 0)	{
			$flagValue = intval("".$this->getFlags());
			$blnSet = (($flagValue & $powerOfTwo) == $powerOfTwo);
		} 
		return $blnSet;
	}
	//Dealing with updates 
	protected function addToUpdateList($key, $value)	{
		//Make it easier to translate to json format in cols
		$index = sizeof($this->l__update);
		$this->l__update[$index] = array();
		$this->l__update[$index][$key] = $value;
	}
	protected function getUpdateList()	{
		return $this->l__update;
	}
	protected function clearUpdateList()	{
		$this->l__update = null;
		$this->l__update = array();
	}
	public function update()	{
		if (sizeof($this->l__update) == 0) throw new Exception("Nothing to update");
		$uArray1 = array();
		$uArray1['query'] = "update";
		$uArray1['table'] = $this->getMyTablename();
		$uArray1['cols'] = $this->l__update;
		$uArray1['where'] = json_decode($this->getIdWhereClause(), true);
		$jresult1 = SQLEngine::execute(json_encode($uArray1), $this->conn);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("Could fetch the status of operation");
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		return $this;
	}
	public function delete()	{
		$uArray1 = array();
		$uArray1['query'] = "delete";
		$uArray1['table'] = $this->getMyTablename();
		$uArray1['where'] = json_decode($this->getIdWhereClause(), true);
		$jresult1 = SQLEngine::execute(json_encode($uArray1), $this->conn);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("Could fetch the status of operation");
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		return $this;
	}
	public abstract function getId();
	public abstract function getId0();
	public abstract function getIdWhereClause();
	public abstract function getId0WhereClause();
    public abstract function setFlags($flags);
	public abstract function getFlags();
	public abstract function getMyClassname();
	public abstract function getMyTablename();	
}
?>