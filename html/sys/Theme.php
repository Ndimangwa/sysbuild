<?php
/******************************************************
**                                                   **
**                 CLASSNAME : Theme                 **
**  Copyright (c) Zoomtong Company Limited           **
**  Developed by : Ndimangwa Fadhili Ngoya           **
**  Timestamp    : 2021:08:01:21:14:04               **
**  Phones       : +255 787 101 808 / 762 357 596    **
**  Email        : ndimangwa@gmail.com               **
**  Address      : P.O BOX 7436 MOSHI, TANZANIA      **
**                                                   **
**  Dedication to my dear wife Valentina             **
**                my daughters Raheli & Keziah       **
**                                                   **
*******************************************************/
class Theme extends __data__ {
	protected $database;
	protected $conn;
	private $themeId;
	private $themeName;
	private $folder;
	private $backgroundColour;
	private $backgroundImage;
	private $fontFace;
	private $fontSize;
	private $fontColour;
	private $extraFilter;
	private $extraInformation;
	private $flags;
/*BEGIN OF CUSTOM CODES : You should Add Your Custom Codes Below this line*/

/*END OF CUSTOM CODES : You should Add Your Custom Codes Above this line*/
	public static function create($database, $id, $conn) { return new Theme($database, $id, $conn); }
	public function __construct($database, $id, $conn)    {
		$this->setMe($database, $id, $conn);
	}
	public function setMe($database, $id, $conn)    {
		$this->database = $database;
		$this->conn = $conn;
		$whereClause = self::getId0Columnname();
		$whereClause = array($whereClause => $id);
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), array('*'), $whereClause);
		$jresult1 = SQLEngine::execute($query, $conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] !== 1) throw new Exception("Duplicate or no record found");
		$resultSet = $jArray1['rows'][0];
		if (! array_key_exists("themeId", $resultSet)) throw new Exception("Column [themeId] not available while pulling data");
		$this->themeId = $resultSet["themeId"];
		if (! array_key_exists("themeName", $resultSet)) throw new Exception("Column [themeName] not available while pulling data");
		$this->setThemeName($resultSet["themeName"]);
		if (! array_key_exists("folder", $resultSet)) throw new Exception("Column [folder] not available while pulling data");
		$this->setFolder($resultSet["folder"]);
		if (! array_key_exists("backgroundColour", $resultSet)) throw new Exception("Column [backgroundColour] not available while pulling data");
		$this->setBackgroundColour($resultSet["backgroundColour"]);
		if (! array_key_exists("backgroundImage", $resultSet)) throw new Exception("Column [backgroundImage] not available while pulling data");
		$this->setBackgroundImage($resultSet["backgroundImage"]);
		if (! array_key_exists("fontFace", $resultSet)) throw new Exception("Column [fontFace] not available while pulling data");
		$this->setFontFace($resultSet["fontFace"]);
		if (! array_key_exists("fontSize", $resultSet)) throw new Exception("Column [fontSize] not available while pulling data");
		$this->setFontSize($resultSet["fontSize"]);
		if (! array_key_exists("fontColour", $resultSet)) throw new Exception("Column [fontColour] not available while pulling data");
		$this->setFontColour($resultSet["fontColour"]);
		if (! array_key_exists("extraFilter", $resultSet)) throw new Exception("Column [extraFilter] not available while pulling data");
		$this->setExtraFilter($resultSet["extraFilter"]);
		if (! array_key_exists("extraInformation", $resultSet)) throw new Exception("Column [extraInformation] not available while pulling data");
		$this->setExtraInformation($resultSet["extraInformation"]);
		if (! array_key_exists("flags", $resultSet)) throw new Exception("Column [flags] not available while pulling data");
		$this->setFlags($resultSet["flags"]);
		$this->clearUpdateList();
		return $this;
	}
	public static function loadAllData($__conn) {
		$colArray1 = array('themeId', 'themeName');
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), $colArray1, null);
		$jresult1 = SQLEngine::execute($query, $__conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		$dataArray1 = array();
		foreach ($jArray1['rows'] as $resultSet)    {
			$index = sizeof($dataArray1); $dataArray1[$index] = array();
			$dataArray1[$index]['__id__'] = $resultSet['themeId'];
			$myval = "";
			$myval .= " ".$resultSet['themeName'];
			$dataArray1[$index]['__name__'] = trim($myval);
		}
		return $dataArray1;
	}
	public function getId() { return md5($this->themeId); }
	public function getIdWhereClause() { return "{ \"themeId\" : $this->themeId }"; }
	public function getId0()  { return $this->themeId; }
	public function getId0WhereClause()  { return "{ \"themeId\" : $this->themeId }"; }
	public function getThemeId(){
		return $this->themeId;
	}
	public function setThemeName($themeName){
		$maxLength = self::getMaximumLength('themeName');
		if (! (is_null($maxLength) || ! (strlen($themeName) > $maxLength))) throw new Exception("[ themeName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('themeName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $themeName) === 1)) throw new Exception("[ themeName ] : ".$regex['message']);
		$this->themeName = $themeName;
		$this->addToUpdateList("themeName", $themeName);
		return $this;
	}
	public function getThemeName(){
		return $this->themeName;
	}
	public function setFolder($folder){
		$maxLength = self::getMaximumLength('folder');
		if (! (is_null($maxLength) || ! (strlen($folder) > $maxLength))) throw new Exception("[ folder ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('folder');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $folder) === 1)) throw new Exception("[ folder ] : ".$regex['message']);
		$this->folder = $folder;
		$this->addToUpdateList("folder", $folder);
		return $this;
	}
	public function getFolder(){
		return $this->folder;
	}
	public function setBackgroundColour($backgroundColour){
		$maxLength = self::getMaximumLength('backgroundColour');
		if (! (is_null($maxLength) || ! (strlen($backgroundColour) > $maxLength))) throw new Exception("[ backgroundColour ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('backgroundColour');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $backgroundColour) === 1)) throw new Exception("[ backgroundColour ] : ".$regex['message']);
		$this->backgroundColour = $backgroundColour;
		$this->addToUpdateList("backgroundColour", $backgroundColour);
		return $this;
	}
	public function getBackgroundColour(){
		return $this->backgroundColour;
	}
	public function setBackgroundImage($backgroundImage){
		$maxLength = self::getMaximumLength('backgroundImage');
		if (! (is_null($maxLength) || ! (strlen($backgroundImage) > $maxLength))) throw new Exception("[ backgroundImage ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('backgroundImage');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $backgroundImage) === 1)) throw new Exception("[ backgroundImage ] : ".$regex['message']);
		$this->backgroundImage = $backgroundImage;
		$this->addToUpdateList("backgroundImage", $backgroundImage);
		return $this;
	}
	public function getBackgroundImage(){
		return $this->backgroundImage;
	}
	public function setFontFace($fontFace){
		$maxLength = self::getMaximumLength('fontFace');
		if (! (is_null($maxLength) || ! (strlen($fontFace) > $maxLength))) throw new Exception("[ fontFace ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('fontFace');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $fontFace) === 1)) throw new Exception("[ fontFace ] : ".$regex['message']);
		$this->fontFace = $fontFace;
		$this->addToUpdateList("fontFace", $fontFace);
		return $this;
	}
	public function getFontFace(){
		return $this->fontFace;
	}
	public function setFontSize($fontSize){
		$maxLength = self::getMaximumLength('fontSize');
		if (! (is_null($maxLength) || ! (strlen($fontSize) > $maxLength))) throw new Exception("[ fontSize ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('fontSize');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $fontSize) === 1)) throw new Exception("[ fontSize ] : ".$regex['message']);
		$this->fontSize = $fontSize;
		$this->addToUpdateList("fontSize", $fontSize);
		return $this;
	}
	public function getFontSize(){
		return $this->fontSize;
	}
	public function setFontColour($fontColour){
		$maxLength = self::getMaximumLength('fontColour');
		if (! (is_null($maxLength) || ! (strlen($fontColour) > $maxLength))) throw new Exception("[ fontColour ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('fontColour');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $fontColour) === 1)) throw new Exception("[ fontColour ] : ".$regex['message']);
		$this->fontColour = $fontColour;
		$this->addToUpdateList("fontColour", $fontColour);
		return $this;
	}
	public function getFontColour(){
		return $this->fontColour;
	}
	public function setExtraFilter($extraFilter){
		$maxLength = self::getMaximumLength('extraFilter');
		if (! (is_null($maxLength) || ! (strlen($extraFilter) > $maxLength))) throw new Exception("[ extraFilter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraFilter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraFilter) === 1)) throw new Exception("[ extraFilter ] : ".$regex['message']);
		$this->extraFilter = $extraFilter;
		$this->addToUpdateList("extraFilter", $extraFilter);
		return $this;
	}
	public function getExtraFilter(){
		return $this->extraFilter;
	}
	public function setExtraInformation($extraInformation){
		$maxLength = self::getMaximumLength('extraInformation');
		if (! (is_null($maxLength) || ! (strlen($extraInformation) > $maxLength))) throw new Exception("[ extraInformation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraInformation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraInformation) === 1)) throw new Exception("[ extraInformation ] : ".$regex['message']);
		$this->extraInformation = $extraInformation;
		$this->addToUpdateList("extraInformation", $extraInformation);
		return $this;
	}
	public function getExtraInformation(){
		return $this->extraInformation;
	}
	public function setFlags($flags){
		$maxLength = self::getMaximumLength('flags');
		if (! (is_null($maxLength) || ! (strlen($flags) > $maxLength))) throw new Exception("[ flags ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('flags');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $flags) === 1)) throw new Exception("[ flags ] : ".$regex['message']);
		$this->flags = $flags;
		$this->addToUpdateList("flags", $flags);
		return $this;
	}
	public function getFlags(){
		return $this->flags;
	}
	public static function getId0Columnname()   { return "themeId"; }
	public static function getIdColumnnames() { return array("themeId"); }
	public static function getReferenceClass($pname)    {
		$tArray1 = array();
		$refclass = null; if (isset($tArray1[$pname])) $refclass = $tArray1[$pname];
		return $refclass;
	}
	public static function getColumnType($pname)    {
		$tArray1 = array('themeId' => 'integer', 'themeName' => 'text', 'folder' => 'text', 'backgroundColour' => 'text', 'backgroundImage' => 'text', 'fontFace' => 'text', 'fontSize' => 'text', 'fontColour' => 'text', 'extraFilter' => 'text', 'extraInformation' => 'text', 'flags' => 'integer');
		$type = null; if (isset($tArray1[$pname])) $type = $tArray1[$pname];
		return $type;
	}
	public static function getRegularExpression($colname)   {
		$tArray1 = array();
		$regexArray1 = null;
		if (isset($tArray1[$colname])) $regexArray1 = $tArray1[$colname];
		return $regexArray1;
	}
	public static function getMaximumLength($colname)    {
		$tArray1 = array();
		$tArray1['themeName'] = 24; 
		$tArray1['folder'] = 16; 
		$tArray1['backgroundColour'] = 6; 
		$tArray1['backgroundImage'] = 16; 
		$tArray1['fontFace'] = 108; 
		$tArray1['fontSize'] = 8; 
		$tArray1['fontColour'] = 6; 
		$tArray1['extraFilter'] = 32; 
		$tArray1['extraInformation'] = 64; 
		$length = null;
		if (isset($tArray1[$colname])) $length = $tArray1[$colname];
		return $length;
	}
	public function getMyClassname()    { return self::getClassname(); }
	public function getMyTablename()    { return self::getTablename(); }
	public function getMyId0Columnname()  { return self::getId0Columnname(); }
	public static function getClassname()  { return "Theme"; }
	public static function getTablename()  { return "_theme"; }
	public static function column2Property($colname)    {
		$tArray1 = array(
			"themeId" => "themeId"
			, "themeName" => "themeName"
			, "folder" => "folder"
			, "backgroundColour" => "backgroundColour"
			, "backgroundImage" => "backgroundImage"
			, "fontFace" => "fontFace"
			, "fontSize" => "fontSize"
			, "fontColour" => "fontColour"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$pname = null;
		if (isset($tArray1[$colname])) $pname = $tArray1[$colname];
		return $pname;
	}
	public static function property2Column($pname)    {
		$tArray1 = array(
			"themeId" => "themeId"
			, "themeName" => "themeName"
			, "folder" => "folder"
			, "backgroundColour" => "backgroundColour"
			, "backgroundImage" => "backgroundImage"
			, "fontFace" => "fontFace"
			, "fontSize" => "fontSize"
			, "fontColour" => "fontColour"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$colname = null;
		if (isset($tArray1[$pname])) $colname = $tArray1[$pname];
		return $colname;
	}
	public static function getColumnLookupTable()   {
		$tArray1 = array();
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "themeId";		$tArray1[$tsize]['pname'] = "themeId";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "themeName";		$tArray1[$tsize]['pname'] = "themeName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "folder";		$tArray1[$tsize]['pname'] = "folder";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "backgroundColour";		$tArray1[$tsize]['pname'] = "backgroundColour";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "backgroundImage";		$tArray1[$tsize]['pname'] = "backgroundImage";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "fontFace";		$tArray1[$tsize]['pname'] = "fontFace";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "fontSize";		$tArray1[$tsize]['pname'] = "fontSize";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "fontColour";		$tArray1[$tsize]['pname'] = "fontColour";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraFilter";		$tArray1[$tsize]['pname'] = "extraFilter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraInformation";		$tArray1[$tsize]['pname'] = "extraInformation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "flags";		$tArray1[$tsize]['pname'] = "flags";
		return $tArray1;
	}
	public static function columnTransitiveMap($pname)  {
		$tArray1 =  array('themeId' => 'themeId', 'themeName' => 'themeName', 'folder' => 'folder', 'backgroundColour' => 'backgroundColour', 'backgroundImage' => 'backgroundImage', 'fontFace' => 'fontFace', 'fontSize' => 'fontSize', 'fontColour' => 'fontColour', 'extraFilter' => 'extraFilter', 'extraInformation' => 'extraInformation', 'flags' => 'flags');
		$pmap = null; if (isset($tArray1[$pname])) $pmap = $tArray1[$pname];
		return $pmap;
	}
	public static function getSearchableColumns()    {
		/* Will return list of Searchable Properties */
		return array('themeName', 'folder', 'backgroundColour', 'backgroundImage', 'fontFace', 'fontSize', 'fontColour', 'extraFilter', 'extraInformation', 'flags');
	}
	public static function getASearchUI($page, $listOfColumnsToDisplay, $optIndex = 0)    {
		$line = "";
		$mycolumnlist = json_encode($listOfColumnsToDisplay);
		$line .= "&lt;div class=&quot;container __ui_search_container__ py-2&quot;&gt;    &lt;div class=&quot;row&quot;&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;    &lt;form id=&quot;__delta_init_basic__&quot;&gt;        &lt;div class=&quot;input-group mb-3&quot;&gt;            &lt;input name=&quot;__ui_search_input__&quot; id=&quot;__ui_search_input__$optIndex&quot; type=&quot;search&quot; data-min-length=&quot;3&quot;                class=&quot;form-control&quot;required placeholder=&quot;Search&quot; aria-label=&quot;Search&quot; aria-describedby=&quot;basic-addon2&quot; /&gt;            &lt;div class=&quot;input-group-append&quot;&gt;                &lt;button id=&quot;__ui_search_button__$optIndex&quot; data-form-id=&quot;__delta_init_basic__&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot;                    data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot;                    data-column='[&quot;themeName&quot;]' data-page='$page' data-class='Theme'                    class=&quot;btn btn-outline-primary btn-perform-search&quot; type=&quot;button&quot;      data-search-input=&quot;text&quot; data-search-input-id=&quot;__ui_search_input__$optIndex&quot; data-toggle=&quot;tooltip&quot;                    title=&quot;This is a basic search&quot;&gt;Search&lt;/button&gt;            &lt;/div&gt;        &lt;/div&gt;    &lt;/form&gt;&lt;/div&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;&lt;button type=&quot;button &quot;class=&quot;btn btn-outline-primary btn-block&quot; name=&quot;__ui_advanced_search_button__&quot; id=&quot;__ui_advanced_search_button__$optIndex&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot; data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot; data-column='[&quot;themeId&quot;,&quot;themeName&quot;,&quot;folder&quot;,&quot;backgroundColour&quot;,&quot;backgroundImage&quot;,&quot;fontFace&quot;,&quot;fontSize&quot;,&quot;fontColour&quot;,&quot;extraFilter&quot;,&quot;extraInformation&quot;,&quot;flags&quot;]' data-min-length=&quot;3&quot; data-page='$page' data-class='Theme' data-search-dialog=&quot;__dialog_search_container_01__&quot; data-toggle=&quot;tooltip&quot; title=&quot;This is a more advanced search technique&quot;&gt;Advanced Search&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;&lt;br/&gt;&lt;div id=&quot;__ui_search_error__$optIndex&quot; class=&quot;p-1 ui-sys-error-message&quot;&gt;&lt;/div&gt;&lt;div style=&quot;overflow-x: scroll;&quot; id=&quot;__ui_search_output_target__$optIndex&quot;&gt;&lt;/div&gt;&lt;/div&gt;";
		$line .= "&lt;script type=&quot;text/javascript&quot;&gt;(function($)    {    $(function()    {        var callbackFunction$optIndex = function(\$button1, data, textStatus, optionArgumentArray1) {            var \$dialog1 = $('#' + \$button1.data('searchDialog'));            \$dialog1 = showAdvancedSearchDialog(\$button1, \$dialog1, data, Constant);            \$dialog1.modal('show');      };        $('#__ui_advanced_search_button__$optIndex').on('click', function(e)   {            var \$button1 = $(this);            var columnList = \$button1.data('column');            var classname = \$button1.data('class');            var payload = { columns: columnList, classname: classname };            fSendAjax(\$button1,                    $('&lt;span/&gt;'),                    '../server/serviceGetAdvancedSearchPayload.php',                    payload,                    null,                    null,                    callbackFunction$optIndex,                    null,                    null,                    &quot;POST&quot;,                    true,                    false,                    &quot;Processing ....&quot;,                    null,                    null);        });    });})(jQuery);&lt;/script&gt;";
		return htmlspecialchars_decode($line);
	}
	public function getMyPropertyValue($pname)  {
		if ($pname == "themeId") return $this->themeId;
		else if ($pname == "themeName") return $this->themeName;
		else if ($pname == "folder") return $this->folder;
		else if ($pname == "backgroundColour") return $this->backgroundColour;
		else if ($pname == "backgroundImage") return $this->backgroundImage;
		else if ($pname == "fontFace") return $this->fontFace;
		else if ($pname == "fontSize") return $this->fontSize;
		else if ($pname == "fontColour") return $this->fontColour;
		else if ($pname == "extraFilter") return $this->extraFilter;
		else if ($pname == "extraInformation") return $this->extraInformation;
		else if ($pname == "flags") return $this->flags;
		return null;
	}
	public static function getValue0Columnname() {
		return "themeName";
	}
	public static function getValueColumnnames()   {
		return array('themeName');
	}
	public function getName() {
		return array($this->themeName);
	}
	public function getName0() {
		$namedValue = $this->themeName;
		return $namedValue;
	}
}
?>