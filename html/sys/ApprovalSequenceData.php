<?php
/******************************************************
**                                                   **
**         CLASSNAME : ApprovalSequenceData          **
**  Copyright (c) Zoomtong Company Limited           **
**  Developed by : Ndimangwa Fadhili Ngoya           **
**  Timestamp    : 2021:08:01:21:14:04               **
**  Phones       : +255 787 101 808 / 762 357 596    **
**  Email        : ndimangwa@gmail.com               **
**  Address      : P.O BOX 7436 MOSHI, TANZANIA      **
**                                                   **
**  Dedication to my dear wife Valentina             **
**                my daughters Raheli & Keziah       **
**                                                   **
*******************************************************/
class ApprovalSequenceData extends __data__ {
	protected $database;
	protected $conn;
	private $dataId;
	private $schema;
	private $requestedBy;
	private $nextJobToApprove;
	private $alreadyApprovedList;
	private $timeOfRegistration;
	private $specialInstruction;
	private $extraFilter;
	private $extraInformation;
	private $flags;
/*BEGIN OF CUSTOM CODES : You should Add Your Custom Codes Below this line*/

/*END OF CUSTOM CODES : You should Add Your Custom Codes Above this line*/
	public static function create($database, $id, $conn) { return new ApprovalSequenceData($database, $id, $conn); }
	public function __construct($database, $id, $conn)    {
		$this->setMe($database, $id, $conn);
	}
	public function setMe($database, $id, $conn)    {
		$this->database = $database;
		$this->conn = $conn;
		$whereClause = self::getId0Columnname();
		$whereClause = array($whereClause => $id);
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), array('*'), $whereClause);
		$jresult1 = SQLEngine::execute($query, $conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] !== 1) throw new Exception("Duplicate or no record found");
		$resultSet = $jArray1['rows'][0];
		if (! array_key_exists("dataId", $resultSet)) throw new Exception("Column [dataId] not available while pulling data");
		$this->dataId = $resultSet["dataId"];
		if (! array_key_exists("schemaId", $resultSet)) throw new Exception("Column [schemaId] not available while pulling data");
		$this->setSchema($resultSet["schemaId"]);
		if (! array_key_exists("requestedBy", $resultSet)) throw new Exception("Column [requestedBy] not available while pulling data");
		$this->setRequestedBy($resultSet["requestedBy"]);
		if (! array_key_exists("nextJobToApprove", $resultSet)) throw new Exception("Column [nextJobToApprove] not available while pulling data");
		$this->setNextJobToApprove($resultSet["nextJobToApprove"]);
		if (! array_key_exists("alreadyApprovedList", $resultSet)) throw new Exception("Column [alreadyApprovedList] not available while pulling data");
		$this->setAlreadyApprovedList($resultSet["alreadyApprovedList"]);
		if (! array_key_exists("timeOfRegistration", $resultSet)) throw new Exception("Column [timeOfRegistration] not available while pulling data");
		$this->setTimeOfRegistration($resultSet["timeOfRegistration"]);
		if (! array_key_exists("specialInstruction", $resultSet)) throw new Exception("Column [specialInstruction] not available while pulling data");
		$this->setSpecialInstruction($resultSet["specialInstruction"]);
		if (! array_key_exists("extraFilter", $resultSet)) throw new Exception("Column [extraFilter] not available while pulling data");
		$this->setExtraFilter($resultSet["extraFilter"]);
		if (! array_key_exists("extraInformation", $resultSet)) throw new Exception("Column [extraInformation] not available while pulling data");
		$this->setExtraInformation($resultSet["extraInformation"]);
		if (! array_key_exists("flags", $resultSet)) throw new Exception("Column [flags] not available while pulling data");
		$this->setFlags($resultSet["flags"]);
		$this->clearUpdateList();
		return $this;
	}
	public function getId() { return md5($this->dataId); }
	public function getIdWhereClause() { return "{ \"dataId\" : $this->dataId }"; }
	public function getId0()  { return $this->dataId; }
	public function getId0WhereClause()  { return "{ \"dataId\" : $this->dataId }"; }
	public function getDataId(){
		return $this->dataId;
	}
	public function setSchema($schemaId){
		$maxLength = self::getMaximumLength('schema');
		if (! (is_null($maxLength) || ! (strlen($schemaId) > $maxLength))) throw new Exception("[ schema ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('schema');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $schemaId) === 1)) throw new Exception("[ schema ] : ".$regex['message']);
		if (is_null($schemaId)) return $this;
		$this->schema = new ApprovalSequenceSchema($this->database, $schemaId, $this->conn);
		$this->addToUpdateList("schemaId", $schemaId);
		return $this;
	}
	public function getSchema(){
		return $this->schema;
	}
	public function setRequestedBy($loginId){
		$maxLength = self::getMaximumLength('requestedBy');
		if (! (is_null($maxLength) || ! (strlen($loginId) > $maxLength))) throw new Exception("[ requestedBy ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('requestedBy');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $loginId) === 1)) throw new Exception("[ requestedBy ] : ".$regex['message']);
		if (is_null($loginId)) return $this;
		$this->requestedBy = new Login($this->database, $loginId, $this->conn);
		$this->addToUpdateList("requestedBy", $loginId);
		return $this;
	}
	public function getRequestedBy(){
		return $this->requestedBy;
	}
	public function setNextJobToApprove($jobId){
		$maxLength = self::getMaximumLength('nextJobToApprove');
		if (! (is_null($maxLength) || ! (strlen($jobId) > $maxLength))) throw new Exception("[ nextJobToApprove ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('nextJobToApprove');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $jobId) === 1)) throw new Exception("[ nextJobToApprove ] : ".$regex['message']);
		if (is_null($jobId)) return $this;
		$this->nextJobToApprove = new JobTitle($this->database, $jobId, $this->conn);
		$this->addToUpdateList("nextJobToApprove", $jobId);
		return $this;
	}
	public function getNextJobToApprove(){
		return $this->nextJobToApprove;
	}
	public function setAlreadyApprovedList($loginId){
		$maxLength = self::getMaximumLength('alreadyApprovedList');
		if (! (is_null($maxLength) || ! (strlen($loginId) > $maxLength))) throw new Exception("[ alreadyApprovedList ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('alreadyApprovedList');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $loginId) === 1)) throw new Exception("[ alreadyApprovedList ] : ".$regex['message']);
		if (is_null($loginId)) return $this;
		$tempArray1 = explode(",", $loginId);
		$this->alreadyApprovedList = array();
		foreach ($tempArray1 as $apropid)  {
			$this->alreadyApprovedList[sizeof($this->alreadyApprovedList)] = new Login($this->database, $apropid, $this->conn);
		}
		$this->addToUpdateList("alreadyApprovedList", $loginId);
		return $this;
	}
	public function getAlreadyApprovedList(){
		return $this->alreadyApprovedList;
	}
	public function setTimeOfRegistration($timeOfRegistration){
		$maxLength = self::getMaximumLength('timeOfRegistration');
		if (! (is_null($maxLength) || ! (strlen($timeOfRegistration) > $maxLength))) throw new Exception("[ timeOfRegistration ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('timeOfRegistration');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $timeOfRegistration) === 1)) throw new Exception("[ timeOfRegistration ] : ".$regex['message']);
		if (is_null($timeOfRegistration)) return $this;
		$this->timeOfRegistration = new DateAndTime($timeOfRegistration);
		$this->addToUpdateList("timeOfRegistration", $timeOfRegistration);
		return $this;
	}
	public function getTimeOfRegistration(){
		return $this->timeOfRegistration;
	}
	public function setSpecialInstruction($specialInstruction){
		$maxLength = self::getMaximumLength('specialInstruction');
		if (! (is_null($maxLength) || ! (strlen($specialInstruction) > $maxLength))) throw new Exception("[ specialInstruction ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('specialInstruction');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $specialInstruction) === 1)) throw new Exception("[ specialInstruction ] : ".$regex['message']);
		$this->specialInstruction = $specialInstruction;
		$this->addToUpdateList("specialInstruction", $specialInstruction);
		return $this;
	}
	public function getSpecialInstruction(){
		return $this->specialInstruction;
	}
	public function setExtraFilter($extraFilter){
		$maxLength = self::getMaximumLength('extraFilter');
		if (! (is_null($maxLength) || ! (strlen($extraFilter) > $maxLength))) throw new Exception("[ extraFilter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraFilter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraFilter) === 1)) throw new Exception("[ extraFilter ] : ".$regex['message']);
		$this->extraFilter = $extraFilter;
		$this->addToUpdateList("extraFilter", $extraFilter);
		return $this;
	}
	public function getExtraFilter(){
		return $this->extraFilter;
	}
	public function setExtraInformation($extraInformation){
		$maxLength = self::getMaximumLength('extraInformation');
		if (! (is_null($maxLength) || ! (strlen($extraInformation) > $maxLength))) throw new Exception("[ extraInformation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraInformation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraInformation) === 1)) throw new Exception("[ extraInformation ] : ".$regex['message']);
		$this->extraInformation = $extraInformation;
		$this->addToUpdateList("extraInformation", $extraInformation);
		return $this;
	}
	public function getExtraInformation(){
		return $this->extraInformation;
	}
	public function setFlags($flags){
		$maxLength = self::getMaximumLength('flags');
		if (! (is_null($maxLength) || ! (strlen($flags) > $maxLength))) throw new Exception("[ flags ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('flags');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $flags) === 1)) throw new Exception("[ flags ] : ".$regex['message']);
		$this->flags = $flags;
		$this->addToUpdateList("flags", $flags);
		return $this;
	}
	public function getFlags(){
		return $this->flags;
	}
	public static function getId0Columnname()   { return "dataId"; }
	public static function getIdColumnnames() { return array("dataId"); }
	public static function getReferenceClass($pname)    {
		$tArray1 = array('schema' => 'ApprovalSequenceSchema', 'requestedBy' => 'Login', 'nextJobToApprove' => 'JobTitle', 'alreadyApprovedList' => 'Login', 'timeOfRegistration' => 'DateAndTime');
		$refclass = null; if (isset($tArray1[$pname])) $refclass = $tArray1[$pname];
		return $refclass;
	}
	public static function getColumnType($pname)    {
		$tArray1 = array('dataId' => 'integer', 'schema' => 'object', 'requestedBy' => 'object', 'nextJobToApprove' => 'object', 'alreadyApprovedList' => 'list-object', 'timeOfRegistration' => 'text', 'specialInstruction' => 'text', 'extraFilter' => 'text', 'extraInformation' => 'text', 'flags' => 'integer');
		$type = null; if (isset($tArray1[$pname])) $type = $tArray1[$pname];
		return $type;
	}
	public static function getRegularExpression($colname)   {
		$tArray1 = array();
		$regexArray1 = null;
		if (isset($tArray1[$colname])) $regexArray1 = $tArray1[$colname];
		return $regexArray1;
	}
	public static function getMaximumLength($colname)    {
		$tArray1 = array();
		$tArray1['alreadyApprovedList'] = 255; 
		$tArray1['specialInstruction'] = 32; 
		$tArray1['extraFilter'] = 32; 
		$tArray1['extraInformation'] = 64; 
		$length = null;
		if (isset($tArray1[$colname])) $length = $tArray1[$colname];
		return $length;
	}
	public function getMyClassname()    { return self::getClassname(); }
	public function getMyTablename()    { return self::getTablename(); }
	public function getMyId0Columnname()  { return self::getId0Columnname(); }
	public static function getClassname()  { return "ApprovalSequenceData"; }
	public static function getTablename()  { return "_approvalSequenceData"; }
	public static function column2Property($colname)    {
		$tArray1 = array(
			"dataId" => "dataId"
			, "schemaId" => "schema"
			, "requestedBy" => "requestedBy"
			, "nextJobToApprove" => "nextJobToApprove"
			, "alreadyApprovedList" => "alreadyApprovedList"
			, "timeOfRegistration" => "timeOfRegistration"
			, "specialInstruction" => "specialInstruction"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$pname = null;
		if (isset($tArray1[$colname])) $pname = $tArray1[$colname];
		return $pname;
	}
	public static function property2Column($pname)    {
		$tArray1 = array(
			"dataId" => "dataId"
			, "schema" => "schemaId"
			, "requestedBy" => "requestedBy"
			, "nextJobToApprove" => "nextJobToApprove"
			, "alreadyApprovedList" => "alreadyApprovedList"
			, "timeOfRegistration" => "timeOfRegistration"
			, "specialInstruction" => "specialInstruction"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$colname = null;
		if (isset($tArray1[$pname])) $colname = $tArray1[$pname];
		return $colname;
	}
	public static function getColumnLookupTable()   {
		$tArray1 = array();
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "dataId";		$tArray1[$tsize]['pname'] = "dataId";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "schemaId";		$tArray1[$tsize]['pname'] = "schema";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "requestedBy";		$tArray1[$tsize]['pname'] = "requestedBy";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "nextJobToApprove";		$tArray1[$tsize]['pname'] = "nextJobToApprove";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "alreadyApprovedList";		$tArray1[$tsize]['pname'] = "alreadyApprovedList";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "timeOfRegistration";		$tArray1[$tsize]['pname'] = "timeOfRegistration";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "specialInstruction";		$tArray1[$tsize]['pname'] = "specialInstruction";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraFilter";		$tArray1[$tsize]['pname'] = "extraFilter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraInformation";		$tArray1[$tsize]['pname'] = "extraInformation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "flags";		$tArray1[$tsize]['pname'] = "flags";
		return $tArray1;
	}
	public static function columnTransitiveMap($pname)  {
		$tArray1 =  array('dataId' => 'dataId', 'schema' => array('ApprovalSequenceSchema.schemaName'), 'requestedBy' => array('Login.loginName'), 'nextJobToApprove' => array('JobTitle.jobName'), 'alreadyApprovedList' => 'alreadyApprovedList', 'timeOfRegistration' => 'timeOfRegistration', 'specialInstruction' => 'specialInstruction', 'extraFilter' => 'extraFilter', 'extraInformation' => 'extraInformation', 'flags' => 'flags');
		$pmap = null; if (isset($tArray1[$pname])) $pmap = $tArray1[$pname];
		return $pmap;
	}
	public static function getSearchableColumns()    {
		/* Will return list of Searchable Properties */
		return array('schema', 'requestedBy', 'nextJobToApprove', 'alreadyApprovedList', 'timeOfRegistration', 'specialInstruction', 'extraFilter', 'extraInformation', 'flags');
	}
	public static function getASearchUI($page, $listOfColumnsToDisplay, $optIndex = 0)    {
		$line = "";
		$mycolumnlist = json_encode($listOfColumnsToDisplay);
		$line .= "&lt;div class=&quot;container __ui_search_container__ py-2&quot;&gt;    &lt;div class=&quot;row&quot;&gt;";
		$line .= "&lt;div class=&quot;col-md-6 offset-md-3&quot;&gt;&lt;button type=&quot;button &quot;class=&quot;btn btn-outline-primary btn-block&quot; name=&quot;__ui_advanced_search_button__&quot; id=&quot;__ui_advanced_search_button__$optIndex&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot; data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot; data-column='[&quot;dataId&quot;,&quot;schema&quot;,&quot;requestedBy&quot;,&quot;nextJobToApprove&quot;,&quot;alreadyApprovedList&quot;,&quot;timeOfRegistration&quot;,&quot;specialInstruction&quot;,&quot;extraFilter&quot;,&quot;extraInformation&quot;,&quot;flags&quot;]' data-min-length=&quot;3&quot; data-page='$page' data-class='ApprovalSequenceData' data-search-dialog=&quot;__dialog_search_container_01__&quot; data-toggle=&quot;tooltip&quot; title=&quot;This is a more advanced search technique&quot;&gt;Advanced Search&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;&lt;br/&gt;&lt;div id=&quot;__ui_search_error__$optIndex&quot; class=&quot;p-1 ui-sys-error-message&quot;&gt;&lt;/div&gt;&lt;div style=&quot;overflow-x: scroll;&quot; id=&quot;__ui_search_output_target__$optIndex&quot;&gt;&lt;/div&gt;&lt;/div&gt;";
		$line .= "&lt;script type=&quot;text/javascript&quot;&gt;(function($)    {    $(function()    {        var callbackFunction$optIndex = function(\$button1, data, textStatus, optionArgumentArray1) {            var \$dialog1 = $('#' + \$button1.data('searchDialog'));            \$dialog1 = showAdvancedSearchDialog(\$button1, \$dialog1, data, Constant);            \$dialog1.modal('show');      };        $('#__ui_advanced_search_button__$optIndex').on('click', function(e)   {            var \$button1 = $(this);            var columnList = \$button1.data('column');            var classname = \$button1.data('class');            var payload = { columns: columnList, classname: classname };            fSendAjax(\$button1,                    $('&lt;span/&gt;'),                    '../server/serviceGetAdvancedSearchPayload.php',                    payload,                    null,                    null,                    callbackFunction$optIndex,                    null,                    null,                    &quot;POST&quot;,                    true,                    false,                    &quot;Processing ....&quot;,                    null,                    null);        });    });})(jQuery);&lt;/script&gt;";
		return htmlspecialchars_decode($line);
	}
	public function getMyPropertyValue($pname)  {
		if ($pname == "dataId") return $this->dataId;
		else if ($pname == "schema") return $this->schema;
		else if ($pname == "requestedBy") return $this->requestedBy;
		else if ($pname == "nextJobToApprove") return $this->nextJobToApprove;
		else if ($pname == "alreadyApprovedList") return $this->alreadyApprovedList;
		else if ($pname == "timeOfRegistration") return $this->timeOfRegistration;
		else if ($pname == "specialInstruction") return $this->specialInstruction;
		else if ($pname == "extraFilter") return $this->extraFilter;
		else if ($pname == "extraInformation") return $this->extraInformation;
		else if ($pname == "flags") return $this->flags;
		return null;
	}
	public static function getValue0Columnname() {
	}
	public static function getValueColumnnames()   {
		return array();
	}
	public function getName() {
		return array();
	}
	public function getName0() {
		$namedValue = null;
		return $namedValue;
	}
}
?>