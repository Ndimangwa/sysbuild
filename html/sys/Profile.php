<?php
/******************************************************
**                                                   **
**                CLASSNAME : Profile                **
**  Copyright (c) Zoomtong Company Limited           **
**  Developed by : Ndimangwa Fadhili Ngoya           **
**  Timestamp    : 2021:08:01:21:14:04               **
**  Phones       : +255 787 101 808 / 762 357 596    **
**  Email        : ndimangwa@gmail.com               **
**  Address      : P.O BOX 7436 MOSHI, TANZANIA      **
**                                                   **
**  Dedication to my dear wife Valentina             **
**                my daughters Raheli & Keziah       **
**                                                   **
*******************************************************/
class Profile extends __data__ {
	protected $database;
	protected $conn;
	private $profileId;
	private $profileName;
	private $systemName;
	private $logo;
	private $telephoneList;
	private $emailList;
	private $otherCommunication;
	private $website;
	private $fax;
	private $city;
	private $country;
	private $PHPTimezone;
	private $dob;
	private $firstDayOfAWeek;
	private $xmlFile;
	private $xmlFileChecksum;
	private $serverIP4Address;
	private $localAreaNetwork4Mask;
	private $maximumNumberOfReturnedSearchRecords;
	private $maximumNumberOfDisplayedRowsPerPage;
	private $minimumAgeCriteriaForUsers;
	private $applicationCounter;
	private $revisionNumber;
	private $revisionTime;
	private $tinNumber;
	private $systemHash;
	private $extraFilter;
	private $extraInformation;
	private $flags;
/*BEGIN OF CUSTOM CODES : You should Add Your Custom Codes Below this line*/

/*END OF CUSTOM CODES : You should Add Your Custom Codes Above this line*/
	public static function create($database, $id, $conn) { return new Profile($database, $id, $conn); }
	public function __construct($database, $id, $conn)    {
		$this->setMe($database, $id, $conn);
	}
	public function setMe($database, $id, $conn)    {
		$this->database = $database;
		$this->conn = $conn;
		$whereClause = self::getId0Columnname();
		$whereClause = array($whereClause => $id);
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), array('*'), $whereClause);
		$jresult1 = SQLEngine::execute($query, $conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] !== 1) throw new Exception("Duplicate or no record found");
		$resultSet = $jArray1['rows'][0];
		if (! array_key_exists("profileId", $resultSet)) throw new Exception("Column [profileId] not available while pulling data");
		$this->profileId = $resultSet["profileId"];
		if (! array_key_exists("profileName", $resultSet)) throw new Exception("Column [profileName] not available while pulling data");
		$this->setProfileName($resultSet["profileName"]);
		if (! array_key_exists("systemName", $resultSet)) throw new Exception("Column [systemName] not available while pulling data");
		$this->setSystemName($resultSet["systemName"]);
		if (! array_key_exists("logo", $resultSet)) throw new Exception("Column [logo] not available while pulling data");
		$this->setLogo($resultSet["logo"]);
		if (! array_key_exists("telephoneList", $resultSet)) throw new Exception("Column [telephoneList] not available while pulling data");
		$this->setTelephoneList($resultSet["telephoneList"]);
		if (! array_key_exists("emailList", $resultSet)) throw new Exception("Column [emailList] not available while pulling data");
		$this->setEmailList($resultSet["emailList"]);
		if (! array_key_exists("otherCommunication", $resultSet)) throw new Exception("Column [otherCommunication] not available while pulling data");
		$this->setOtherCommunication($resultSet["otherCommunication"]);
		if (! array_key_exists("website", $resultSet)) throw new Exception("Column [website] not available while pulling data");
		$this->setWebsite($resultSet["website"]);
		if (! array_key_exists("fax", $resultSet)) throw new Exception("Column [fax] not available while pulling data");
		$this->setFax($resultSet["fax"]);
		if (! array_key_exists("city", $resultSet)) throw new Exception("Column [city] not available while pulling data");
		$this->setCity($resultSet["city"]);
		if (! array_key_exists("countryId", $resultSet)) throw new Exception("Column [countryId] not available while pulling data");
		$this->setCountry($resultSet["countryId"]);
		if (! array_key_exists("pHPTimezoneId", $resultSet)) throw new Exception("Column [pHPTimezoneId] not available while pulling data");
		$this->setPHPTimezone($resultSet["pHPTimezoneId"]);
		if (! array_key_exists("dob", $resultSet)) throw new Exception("Column [dob] not available while pulling data");
		$this->setDob($resultSet["dob"]);
		if (! array_key_exists("firstDayOfAWeekId", $resultSet)) throw new Exception("Column [firstDayOfAWeekId] not available while pulling data");
		$this->setFirstDayOfAWeek($resultSet["firstDayOfAWeekId"]);
		if (! array_key_exists("xmlFile", $resultSet)) throw new Exception("Column [xmlFile] not available while pulling data");
		$this->setXmlFile($resultSet["xmlFile"]);
		if (! array_key_exists("xmlFileChecksum", $resultSet)) throw new Exception("Column [xmlFileChecksum] not available while pulling data");
		$this->setXmlFileChecksum($resultSet["xmlFileChecksum"]);
		if (! array_key_exists("serverIP4Address", $resultSet)) throw new Exception("Column [serverIP4Address] not available while pulling data");
		$this->setServerIP4Address($resultSet["serverIP4Address"]);
		if (! array_key_exists("localAreaNetwork4Mask", $resultSet)) throw new Exception("Column [localAreaNetwork4Mask] not available while pulling data");
		$this->setLocalAreaNetwork4Mask($resultSet["localAreaNetwork4Mask"]);
		if (! array_key_exists("maxNumberOfReturnedSearchRecords", $resultSet)) throw new Exception("Column [maxNumberOfReturnedSearchRecords] not available while pulling data");
		$this->setMaximumNumberOfReturnedSearchRecords($resultSet["maxNumberOfReturnedSearchRecords"]);
		if (! array_key_exists("maxNumberOfDisplayedRowsPerPage", $resultSet)) throw new Exception("Column [maxNumberOfDisplayedRowsPerPage] not available while pulling data");
		$this->setMaximumNumberOfDisplayedRowsPerPage($resultSet["maxNumberOfDisplayedRowsPerPage"]);
		if (! array_key_exists("minAgeCriteriaForUsers", $resultSet)) throw new Exception("Column [minAgeCriteriaForUsers] not available while pulling data");
		$this->setMinimumAgeCriteriaForUsers($resultSet["minAgeCriteriaForUsers"]);
		if (! array_key_exists("applicationCounter", $resultSet)) throw new Exception("Column [applicationCounter] not available while pulling data");
		$this->setApplicationCounter($resultSet["applicationCounter"]);
		if (! array_key_exists("revisionNumber", $resultSet)) throw new Exception("Column [revisionNumber] not available while pulling data");
		$this->setRevisionNumber($resultSet["revisionNumber"]);
		if (! array_key_exists("revisionTime", $resultSet)) throw new Exception("Column [revisionTime] not available while pulling data");
		$this->setRevisionTime($resultSet["revisionTime"]);
		if (! array_key_exists("tinNumber", $resultSet)) throw new Exception("Column [tinNumber] not available while pulling data");
		$this->setTinNumber($resultSet["tinNumber"]);
		if (! array_key_exists("systemHash", $resultSet)) throw new Exception("Column [systemHash] not available while pulling data");
		$this->setSystemHash($resultSet["systemHash"]);
		if (! array_key_exists("extraFilter", $resultSet)) throw new Exception("Column [extraFilter] not available while pulling data");
		$this->setExtraFilter($resultSet["extraFilter"]);
		if (! array_key_exists("extraInformation", $resultSet)) throw new Exception("Column [extraInformation] not available while pulling data");
		$this->setExtraInformation($resultSet["extraInformation"]);
		if (! array_key_exists("flags", $resultSet)) throw new Exception("Column [flags] not available while pulling data");
		$this->setFlags($resultSet["flags"]);
		$this->clearUpdateList();
		return $this;
	}
	public static function loadAllData($__conn) {
		$colArray1 = array('profileId', 'profileName');
		$query = SimpleQueryBuilder::buildSelect(array(self::getTablename()), $colArray1, null);
		$jresult1 = SQLEngine::execute($query, $__conn);
		$jArray1 = json_decode($jresult1, true);
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		$dataArray1 = array();
		foreach ($jArray1['rows'] as $resultSet)    {
			$index = sizeof($dataArray1); $dataArray1[$index] = array();
			$dataArray1[$index]['__id__'] = $resultSet['profileId'];
			$myval = "";
			$myval .= " ".$resultSet['profileName'];
			$dataArray1[$index]['__name__'] = trim($myval);
		}
		return $dataArray1;
	}
	public function getId() { return md5($this->profileId); }
	public function getIdWhereClause() { return "{ \"profileId\" : $this->profileId }"; }
	public function getId0()  { return $this->profileId; }
	public function getId0WhereClause()  { return "{ \"profileId\" : $this->profileId }"; }
	public function getProfileId(){
		return $this->profileId;
	}
	public function setProfileName($profileName){
		$maxLength = self::getMaximumLength('profileName');
		if (! (is_null($maxLength) || ! (strlen($profileName) > $maxLength))) throw new Exception("[ profileName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('profileName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $profileName) === 1)) throw new Exception("[ profileName ] : ".$regex['message']);
		$this->profileName = $profileName;
		$this->addToUpdateList("profileName", $profileName);
		return $this;
	}
	public function getProfileName(){
		return $this->profileName;
	}
	public function setSystemName($systemName){
		$maxLength = self::getMaximumLength('systemName');
		if (! (is_null($maxLength) || ! (strlen($systemName) > $maxLength))) throw new Exception("[ systemName ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('systemName');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $systemName) === 1)) throw new Exception("[ systemName ] : ".$regex['message']);
		$this->systemName = $systemName;
		$this->addToUpdateList("systemName", $systemName);
		return $this;
	}
	public function getSystemName(){
		return $this->systemName;
	}
	public function setLogo($logo){
		$maxLength = self::getMaximumLength('logo');
		if (! (is_null($maxLength) || ! (strlen($logo) > $maxLength))) throw new Exception("[ logo ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('logo');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $logo) === 1)) throw new Exception("[ logo ] : ".$regex['message']);
		$this->logo = $logo;
		$this->addToUpdateList("logo", $logo);
		return $this;
	}
	public function getLogo(){
		return $this->logo;
	}
	public function setTelephoneList($telephoneList){
		$maxLength = self::getMaximumLength('telephoneList');
		if (! (is_null($maxLength) || ! (strlen($telephoneList) > $maxLength))) throw new Exception("[ telephoneList ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('telephoneList');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $telephoneList) === 1)) throw new Exception("[ telephoneList ] : ".$regex['message']);
		$this->telephoneList = $telephoneList;
		$this->addToUpdateList("telephoneList", $telephoneList);
		return $this;
	}
	public function getTelephoneList(){
		return $this->telephoneList;
	}
	public function setEmailList($emailList){
		$maxLength = self::getMaximumLength('emailList');
		if (! (is_null($maxLength) || ! (strlen($emailList) > $maxLength))) throw new Exception("[ emailList ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('emailList');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $emailList) === 1)) throw new Exception("[ emailList ] : ".$regex['message']);
		$this->emailList = $emailList;
		$this->addToUpdateList("emailList", $emailList);
		return $this;
	}
	public function getEmailList(){
		return $this->emailList;
	}
	public function setOtherCommunication($otherCommunication){
		$maxLength = self::getMaximumLength('otherCommunication');
		if (! (is_null($maxLength) || ! (strlen($otherCommunication) > $maxLength))) throw new Exception("[ otherCommunication ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('otherCommunication');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $otherCommunication) === 1)) throw new Exception("[ otherCommunication ] : ".$regex['message']);
		$this->otherCommunication = $otherCommunication;
		$this->addToUpdateList("otherCommunication", $otherCommunication);
		return $this;
	}
	public function getOtherCommunication(){
		return $this->otherCommunication;
	}
	public function setWebsite($website){
		$maxLength = self::getMaximumLength('website');
		if (! (is_null($maxLength) || ! (strlen($website) > $maxLength))) throw new Exception("[ website ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('website');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $website) === 1)) throw new Exception("[ website ] : ".$regex['message']);
		$this->website = $website;
		$this->addToUpdateList("website", $website);
		return $this;
	}
	public function getWebsite(){
		return $this->website;
	}
	public function setFax($fax){
		$maxLength = self::getMaximumLength('fax');
		if (! (is_null($maxLength) || ! (strlen($fax) > $maxLength))) throw new Exception("[ fax ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('fax');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $fax) === 1)) throw new Exception("[ fax ] : ".$regex['message']);
		$this->fax = $fax;
		$this->addToUpdateList("fax", $fax);
		return $this;
	}
	public function getFax(){
		return $this->fax;
	}
	public function setCity($city){
		$maxLength = self::getMaximumLength('city');
		if (! (is_null($maxLength) || ! (strlen($city) > $maxLength))) throw new Exception("[ city ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('city');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $city) === 1)) throw new Exception("[ city ] : ".$regex['message']);
		$this->city = $city;
		$this->addToUpdateList("city", $city);
		return $this;
	}
	public function getCity(){
		return $this->city;
	}
	public function setCountry($countryId){
		$maxLength = self::getMaximumLength('country');
		if (! (is_null($maxLength) || ! (strlen($countryId) > $maxLength))) throw new Exception("[ country ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('country');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $countryId) === 1)) throw new Exception("[ country ] : ".$regex['message']);
		if (is_null($countryId)) return $this;
		$this->country = new Country($this->database, $countryId, $this->conn);
		$this->addToUpdateList("countryId", $countryId);
		return $this;
	}
	public function getCountry(){
		return $this->country;
	}
	public function setPHPTimezone($zoneId){
		$maxLength = self::getMaximumLength('PHPTimezone');
		if (! (is_null($maxLength) || ! (strlen($zoneId) > $maxLength))) throw new Exception("[ PHPTimezone ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('PHPTimezone');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $zoneId) === 1)) throw new Exception("[ PHPTimezone ] : ".$regex['message']);
		if (is_null($zoneId)) return $this;
		$this->PHPTimezone = new PHPTimezone($this->database, $zoneId, $this->conn);
		$this->addToUpdateList("pHPTimezoneId", $zoneId);
		return $this;
	}
	public function getPHPTimezone(){
		return $this->PHPTimezone;
	}
	public function setDob($dob){
		$maxLength = self::getMaximumLength('dob');
		if (! (is_null($maxLength) || ! (strlen($dob) > $maxLength))) throw new Exception("[ dob ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('dob');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $dob) === 1)) throw new Exception("[ dob ] : ".$regex['message']);
		if (is_null($dob)) return $this;
		$this->dob = new DateAndTime($dob);
		$this->addToUpdateList("dob", $dob);
		return $this;
	}
	public function getDob(){
		return $this->dob;
	}
	public function setFirstDayOfAWeek($dayId){
		$maxLength = self::getMaximumLength('firstDayOfAWeek');
		if (! (is_null($maxLength) || ! (strlen($dayId) > $maxLength))) throw new Exception("[ firstDayOfAWeek ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('firstDayOfAWeek');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $dayId) === 1)) throw new Exception("[ firstDayOfAWeek ] : ".$regex['message']);
		if (is_null($dayId)) return $this;
		$this->firstDayOfAWeek = new DaysOfAWeek($this->database, $dayId, $this->conn);
		$this->addToUpdateList("firstDayOfAWeekId", $dayId);
		return $this;
	}
	public function getFirstDayOfAWeek(){
		return $this->firstDayOfAWeek;
	}
	public function setXmlFile($xmlFile){
		$maxLength = self::getMaximumLength('xmlFile');
		if (! (is_null($maxLength) || ! (strlen($xmlFile) > $maxLength))) throw new Exception("[ xmlFile ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('xmlFile');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $xmlFile) === 1)) throw new Exception("[ xmlFile ] : ".$regex['message']);
		$this->xmlFile = $xmlFile;
		$this->addToUpdateList("xmlFile", $xmlFile);
		return $this;
	}
	public function getXmlFile(){
		return $this->xmlFile;
	}
	public function setXmlFileChecksum($xmlFileChecksum){
		$maxLength = self::getMaximumLength('xmlFileChecksum');
		if (! (is_null($maxLength) || ! (strlen($xmlFileChecksum) > $maxLength))) throw new Exception("[ xmlFileChecksum ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('xmlFileChecksum');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $xmlFileChecksum) === 1)) throw new Exception("[ xmlFileChecksum ] : ".$regex['message']);
		$this->xmlFileChecksum = $xmlFileChecksum;
		$this->addToUpdateList("xmlFileChecksum", $xmlFileChecksum);
		return $this;
	}
	public function getXmlFileChecksum(){
		return $this->xmlFileChecksum;
	}
	public function setServerIP4Address($serverIP4Address){
		$maxLength = self::getMaximumLength('serverIP4Address');
		if (! (is_null($maxLength) || ! (strlen($serverIP4Address) > $maxLength))) throw new Exception("[ serverIP4Address ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('serverIP4Address');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $serverIP4Address) === 1)) throw new Exception("[ serverIP4Address ] : ".$regex['message']);
		$this->serverIP4Address = $serverIP4Address;
		$this->addToUpdateList("serverIP4Address", $serverIP4Address);
		return $this;
	}
	public function getServerIP4Address(){
		return $this->serverIP4Address;
	}
	public function setLocalAreaNetwork4Mask($localAreaNetwork4Mask){
		$maxLength = self::getMaximumLength('localAreaNetwork4Mask');
		if (! (is_null($maxLength) || ! (strlen($localAreaNetwork4Mask) > $maxLength))) throw new Exception("[ localAreaNetwork4Mask ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('localAreaNetwork4Mask');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $localAreaNetwork4Mask) === 1)) throw new Exception("[ localAreaNetwork4Mask ] : ".$regex['message']);
		$this->localAreaNetwork4Mask = $localAreaNetwork4Mask;
		$this->addToUpdateList("localAreaNetwork4Mask", $localAreaNetwork4Mask);
		return $this;
	}
	public function getLocalAreaNetwork4Mask(){
		return $this->localAreaNetwork4Mask;
	}
	public function setMaximumNumberOfReturnedSearchRecords($maximumNumberOfReturnedSearchRecords){
		$maxLength = self::getMaximumLength('maximumNumberOfReturnedSearchRecords');
		if (! (is_null($maxLength) || ! (strlen($maximumNumberOfReturnedSearchRecords) > $maxLength))) throw new Exception("[ maximumNumberOfReturnedSearchRecords ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('maximumNumberOfReturnedSearchRecords');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $maximumNumberOfReturnedSearchRecords) === 1)) throw new Exception("[ maximumNumberOfReturnedSearchRecords ] : ".$regex['message']);
		$this->maximumNumberOfReturnedSearchRecords = $maximumNumberOfReturnedSearchRecords;
		$this->addToUpdateList("maxNumberOfReturnedSearchRecords", $maximumNumberOfReturnedSearchRecords);
		return $this;
	}
	public function getMaximumNumberOfReturnedSearchRecords(){
		return $this->maximumNumberOfReturnedSearchRecords;
	}
	public function setMaximumNumberOfDisplayedRowsPerPage($maximumNumberOfDisplayedRowsPerPage){
		$maxLength = self::getMaximumLength('maximumNumberOfDisplayedRowsPerPage');
		if (! (is_null($maxLength) || ! (strlen($maximumNumberOfDisplayedRowsPerPage) > $maxLength))) throw new Exception("[ maximumNumberOfDisplayedRowsPerPage ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('maximumNumberOfDisplayedRowsPerPage');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $maximumNumberOfDisplayedRowsPerPage) === 1)) throw new Exception("[ maximumNumberOfDisplayedRowsPerPage ] : ".$regex['message']);
		$this->maximumNumberOfDisplayedRowsPerPage = $maximumNumberOfDisplayedRowsPerPage;
		$this->addToUpdateList("maxNumberOfDisplayedRowsPerPage", $maximumNumberOfDisplayedRowsPerPage);
		return $this;
	}
	public function getMaximumNumberOfDisplayedRowsPerPage(){
		return $this->maximumNumberOfDisplayedRowsPerPage;
	}
	public function setMinimumAgeCriteriaForUsers($minimumAgeCriteriaForUsers){
		$maxLength = self::getMaximumLength('minimumAgeCriteriaForUsers');
		if (! (is_null($maxLength) || ! (strlen($minimumAgeCriteriaForUsers) > $maxLength))) throw new Exception("[ minimumAgeCriteriaForUsers ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('minimumAgeCriteriaForUsers');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $minimumAgeCriteriaForUsers) === 1)) throw new Exception("[ minimumAgeCriteriaForUsers ] : ".$regex['message']);
		$this->minimumAgeCriteriaForUsers = $minimumAgeCriteriaForUsers;
		$this->addToUpdateList("minAgeCriteriaForUsers", $minimumAgeCriteriaForUsers);
		return $this;
	}
	public function getMinimumAgeCriteriaForUsers(){
		return $this->minimumAgeCriteriaForUsers;
	}
	public function setApplicationCounter($applicationCounter){
		$maxLength = self::getMaximumLength('applicationCounter');
		if (! (is_null($maxLength) || ! (strlen($applicationCounter) > $maxLength))) throw new Exception("[ applicationCounter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('applicationCounter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $applicationCounter) === 1)) throw new Exception("[ applicationCounter ] : ".$regex['message']);
		$this->applicationCounter = $applicationCounter;
		$this->addToUpdateList("applicationCounter", $applicationCounter);
		return $this;
	}
	public function getApplicationCounter(){
		return $this->applicationCounter;
	}
	public function setRevisionNumber($revisionNumber){
		$maxLength = self::getMaximumLength('revisionNumber');
		if (! (is_null($maxLength) || ! (strlen($revisionNumber) > $maxLength))) throw new Exception("[ revisionNumber ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('revisionNumber');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $revisionNumber) === 1)) throw new Exception("[ revisionNumber ] : ".$regex['message']);
		$this->revisionNumber = $revisionNumber;
		$this->addToUpdateList("revisionNumber", $revisionNumber);
		return $this;
	}
	public function getRevisionNumber(){
		return $this->revisionNumber;
	}
	public function setRevisionTime($revisionTime){
		$maxLength = self::getMaximumLength('revisionTime');
		if (! (is_null($maxLength) || ! (strlen($revisionTime) > $maxLength))) throw new Exception("[ revisionTime ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('revisionTime');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $revisionTime) === 1)) throw new Exception("[ revisionTime ] : ".$regex['message']);
		if (is_null($revisionTime)) return $this;
		$this->revisionTime = new DateAndTime($revisionTime);
		$this->addToUpdateList("revisionTime", $revisionTime);
		return $this;
	}
	public function getRevisionTime(){
		return $this->revisionTime;
	}
	public function setTinNumber($tinNumber){
		$maxLength = self::getMaximumLength('tinNumber');
		if (! (is_null($maxLength) || ! (strlen($tinNumber) > $maxLength))) throw new Exception("[ tinNumber ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('tinNumber');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $tinNumber) === 1)) throw new Exception("[ tinNumber ] : ".$regex['message']);
		$this->tinNumber = $tinNumber;
		$this->addToUpdateList("tinNumber", $tinNumber);
		return $this;
	}
	public function getTinNumber(){
		return $this->tinNumber;
	}
	public function setSystemHash($systemHash){
		$maxLength = self::getMaximumLength('systemHash');
		if (! (is_null($maxLength) || ! (strlen($systemHash) > $maxLength))) throw new Exception("[ systemHash ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('systemHash');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $systemHash) === 1)) throw new Exception("[ systemHash ] : ".$regex['message']);
		$this->systemHash = $systemHash;
		$this->addToUpdateList("systemHash", $systemHash);
		return $this;
	}
	public function getSystemHash(){
		return $this->systemHash;
	}
	public function setExtraFilter($extraFilter){
		$maxLength = self::getMaximumLength('extraFilter');
		if (! (is_null($maxLength) || ! (strlen($extraFilter) > $maxLength))) throw new Exception("[ extraFilter ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraFilter');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraFilter) === 1)) throw new Exception("[ extraFilter ] : ".$regex['message']);
		$this->extraFilter = $extraFilter;
		$this->addToUpdateList("extraFilter", $extraFilter);
		return $this;
	}
	public function getExtraFilter(){
		return $this->extraFilter;
	}
	public function setExtraInformation($extraInformation){
		$maxLength = self::getMaximumLength('extraInformation');
		if (! (is_null($maxLength) || ! (strlen($extraInformation) > $maxLength))) throw new Exception("[ extraInformation ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('extraInformation');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $extraInformation) === 1)) throw new Exception("[ extraInformation ] : ".$regex['message']);
		$this->extraInformation = $extraInformation;
		$this->addToUpdateList("extraInformation", $extraInformation);
		return $this;
	}
	public function getExtraInformation(){
		return $this->extraInformation;
	}
	public function setFlags($flags){
		$maxLength = self::getMaximumLength('flags');
		if (! (is_null($maxLength) || ! (strlen($flags) > $maxLength))) throw new Exception("[ flags ($maxLength) ] : Data Length has exceeded the size");
		$regex = self::getRegularExpression('flags');
		if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $flags) === 1)) throw new Exception("[ flags ] : ".$regex['message']);
		$this->flags = $flags;
		$this->addToUpdateList("flags", $flags);
		return $this;
	}
	public function getFlags(){
		return $this->flags;
	}
	public static function getId0Columnname()   { return "profileId"; }
	public static function getIdColumnnames() { return array("profileId"); }
	public static function getReferenceClass($pname)    {
		$tArray1 = array('country' => 'Country', 'PHPTimezone' => 'PHPTimezone', 'dob' => 'DateAndTime', 'firstDayOfAWeek' => 'DaysOfAWeek', 'revisionTime' => 'DateAndTime');
		$refclass = null; if (isset($tArray1[$pname])) $refclass = $tArray1[$pname];
		return $refclass;
	}
	public static function getColumnType($pname)    {
		$tArray1 = array('profileId' => 'integer', 'profileName' => 'text', 'systemName' => 'text', 'logo' => 'text', 'telephoneList' => 'text', 'emailList' => 'text', 'otherCommunication' => 'text', 'website' => 'text', 'fax' => 'text', 'city' => 'text', 'country' => 'object', 'PHPTimezone' => 'object', 'dob' => 'text', 'firstDayOfAWeek' => 'object', 'xmlFile' => 'text', 'xmlFileChecksum' => 'text', 'serverIP4Address' => 'text', 'localAreaNetwork4Mask' => 'text', 'maximumNumberOfReturnedSearchRecords' => 'integer', 'maximumNumberOfDisplayedRowsPerPage' => 'integer', 'minimumAgeCriteriaForUsers' => 'integer', 'applicationCounter' => 'integer', 'revisionNumber' => 'integer', 'revisionTime' => 'text', 'tinNumber' => 'text', 'systemHash' => 'text', 'extraFilter' => 'text', 'extraInformation' => 'text', 'flags' => 'integer');
		$type = null; if (isset($tArray1[$pname])) $type = $tArray1[$pname];
		return $type;
	}
	public static function getRegularExpression($colname)   {
		$tArray1 = array();
		$regexArray1 = null;
		if (isset($tArray1[$colname])) $regexArray1 = $tArray1[$colname];
		return $regexArray1;
	}
	public static function getMaximumLength($colname)    {
		$tArray1 = array();
		$tArray1['profileName'] = 96; 
		$tArray1['systemName'] = 108; 
		$tArray1['logo'] = 64; 
		$tArray1['telephoneList'] = 255; 
		$tArray1['emailList'] = 255; 
		$tArray1['otherCommunication'] = 255; 
		$tArray1['website'] = 64; 
		$tArray1['fax'] = 32; 
		$tArray1['city'] = 32; 
		$tArray1['dob'] = 19; 
		$tArray1['xmlFile'] = 64; 
		$tArray1['xmlFileChecksum'] = 32; 
		$tArray1['serverIP4Address'] = 15; 
		$tArray1['localAreaNetwork4Mask'] = 15; 
		$tArray1['revisionTime'] = 19; 
		$tArray1['tinNumber'] = 16; 
		$tArray1['systemHash'] = 32; 
		$tArray1['extraFilter'] = 32; 
		$tArray1['extraInformation'] = 64; 
		$length = null;
		if (isset($tArray1[$colname])) $length = $tArray1[$colname];
		return $length;
	}
	public function getMyClassname()    { return self::getClassname(); }
	public function getMyTablename()    { return self::getTablename(); }
	public function getMyId0Columnname()  { return self::getId0Columnname(); }
	public static function getClassname()  { return "Profile"; }
	public static function getTablename()  { return "_profile"; }
	public static function column2Property($colname)    {
		$tArray1 = array(
			"profileId" => "profileId"
			, "profileName" => "profileName"
			, "systemName" => "systemName"
			, "logo" => "logo"
			, "telephoneList" => "telephoneList"
			, "emailList" => "emailList"
			, "otherCommunication" => "otherCommunication"
			, "website" => "website"
			, "fax" => "fax"
			, "city" => "city"
			, "countryId" => "country"
			, "pHPTimezoneId" => "PHPTimezone"
			, "dob" => "dob"
			, "firstDayOfAWeekId" => "firstDayOfAWeek"
			, "xmlFile" => "xmlFile"
			, "xmlFileChecksum" => "xmlFileChecksum"
			, "serverIP4Address" => "serverIP4Address"
			, "localAreaNetwork4Mask" => "localAreaNetwork4Mask"
			, "maxNumberOfReturnedSearchRecords" => "maximumNumberOfReturnedSearchRecords"
			, "maxNumberOfDisplayedRowsPerPage" => "maximumNumberOfDisplayedRowsPerPage"
			, "minAgeCriteriaForUsers" => "minimumAgeCriteriaForUsers"
			, "applicationCounter" => "applicationCounter"
			, "revisionNumber" => "revisionNumber"
			, "revisionTime" => "revisionTime"
			, "tinNumber" => "tinNumber"
			, "systemHash" => "systemHash"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$pname = null;
		if (isset($tArray1[$colname])) $pname = $tArray1[$colname];
		return $pname;
	}
	public static function property2Column($pname)    {
		$tArray1 = array(
			"profileId" => "profileId"
			, "profileName" => "profileName"
			, "systemName" => "systemName"
			, "logo" => "logo"
			, "telephoneList" => "telephoneList"
			, "emailList" => "emailList"
			, "otherCommunication" => "otherCommunication"
			, "website" => "website"
			, "fax" => "fax"
			, "city" => "city"
			, "country" => "countryId"
			, "PHPTimezone" => "pHPTimezoneId"
			, "dob" => "dob"
			, "firstDayOfAWeek" => "firstDayOfAWeekId"
			, "xmlFile" => "xmlFile"
			, "xmlFileChecksum" => "xmlFileChecksum"
			, "serverIP4Address" => "serverIP4Address"
			, "localAreaNetwork4Mask" => "localAreaNetwork4Mask"
			, "maximumNumberOfReturnedSearchRecords" => "maxNumberOfReturnedSearchRecords"
			, "maximumNumberOfDisplayedRowsPerPage" => "maxNumberOfDisplayedRowsPerPage"
			, "minimumAgeCriteriaForUsers" => "minAgeCriteriaForUsers"
			, "applicationCounter" => "applicationCounter"
			, "revisionNumber" => "revisionNumber"
			, "revisionTime" => "revisionTime"
			, "tinNumber" => "tinNumber"
			, "systemHash" => "systemHash"
			, "extraFilter" => "extraFilter"
			, "extraInformation" => "extraInformation"
			, "flags" => "flags"
		);
		$colname = null;
		if (isset($tArray1[$pname])) $colname = $tArray1[$pname];
		return $colname;
	}
	public static function getColumnLookupTable()   {
		$tArray1 = array();
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "profileId";		$tArray1[$tsize]['pname'] = "profileId";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "profileName";		$tArray1[$tsize]['pname'] = "profileName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "systemName";		$tArray1[$tsize]['pname'] = "systemName";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "logo";		$tArray1[$tsize]['pname'] = "logo";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "telephoneList";		$tArray1[$tsize]['pname'] = "telephoneList";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "emailList";		$tArray1[$tsize]['pname'] = "emailList";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "otherCommunication";		$tArray1[$tsize]['pname'] = "otherCommunication";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "website";		$tArray1[$tsize]['pname'] = "website";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "fax";		$tArray1[$tsize]['pname'] = "fax";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "city";		$tArray1[$tsize]['pname'] = "city";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "countryId";		$tArray1[$tsize]['pname'] = "country";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "pHPTimezoneId";		$tArray1[$tsize]['pname'] = "PHPTimezone";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "dob";		$tArray1[$tsize]['pname'] = "dob";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "firstDayOfAWeekId";		$tArray1[$tsize]['pname'] = "firstDayOfAWeek";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "xmlFile";		$tArray1[$tsize]['pname'] = "xmlFile";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "xmlFileChecksum";		$tArray1[$tsize]['pname'] = "xmlFileChecksum";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "serverIP4Address";		$tArray1[$tsize]['pname'] = "serverIP4Address";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "localAreaNetwork4Mask";		$tArray1[$tsize]['pname'] = "localAreaNetwork4Mask";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "maxNumberOfReturnedSearchRecords";		$tArray1[$tsize]['pname'] = "maximumNumberOfReturnedSearchRecords";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "maxNumberOfDisplayedRowsPerPage";		$tArray1[$tsize]['pname'] = "maximumNumberOfDisplayedRowsPerPage";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "minAgeCriteriaForUsers";		$tArray1[$tsize]['pname'] = "minimumAgeCriteriaForUsers";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "applicationCounter";		$tArray1[$tsize]['pname'] = "applicationCounter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "revisionNumber";		$tArray1[$tsize]['pname'] = "revisionNumber";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "revisionTime";		$tArray1[$tsize]['pname'] = "revisionTime";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "tinNumber";		$tArray1[$tsize]['pname'] = "tinNumber";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "systemHash";		$tArray1[$tsize]['pname'] = "systemHash";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraFilter";		$tArray1[$tsize]['pname'] = "extraFilter";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "extraInformation";		$tArray1[$tsize]['pname'] = "extraInformation";
		$tsize = sizeof($tArray1);		$tArray1[$tsize] = array();		$tArray1[$tsize]['colname'] = "flags";		$tArray1[$tsize]['pname'] = "flags";
		return $tArray1;
	}
	public static function columnTransitiveMap($pname)  {
		$tArray1 =  array('profileId' => 'profileId', 'profileName' => 'profileName', 'systemName' => 'systemName', 'logo' => 'logo', 'telephoneList' => 'telephoneList', 'emailList' => 'emailList', 'otherCommunication' => 'otherCommunication', 'website' => 'website', 'fax' => 'fax', 'city' => 'city', 'country' => array('Country.countryName'), 'PHPTimezone' => array('PHPTimezone.zoneName'), 'dob' => 'dob', 'firstDayOfAWeek' => array('DaysOfAWeek.dayName'), 'xmlFile' => 'xmlFile', 'xmlFileChecksum' => 'xmlFileChecksum', 'serverIP4Address' => 'serverIP4Address', 'localAreaNetwork4Mask' => 'localAreaNetwork4Mask', 'maximumNumberOfReturnedSearchRecords' => 'maximumNumberOfReturnedSearchRecords', 'maximumNumberOfDisplayedRowsPerPage' => 'maximumNumberOfDisplayedRowsPerPage', 'minimumAgeCriteriaForUsers' => 'minimumAgeCriteriaForUsers', 'applicationCounter' => 'applicationCounter', 'revisionNumber' => 'revisionNumber', 'revisionTime' => 'revisionTime', 'tinNumber' => 'tinNumber', 'systemHash' => 'systemHash', 'extraFilter' => 'extraFilter', 'extraInformation' => 'extraInformation', 'flags' => 'flags');
		$pmap = null; if (isset($tArray1[$pname])) $pmap = $tArray1[$pname];
		return $pmap;
	}
	public static function getSearchableColumns()    {
		/* Will return list of Searchable Properties */
		return array('profileName', 'systemName', 'logo', 'telephoneList', 'emailList', 'otherCommunication', 'website', 'fax', 'city', 'country', 'PHPTimezone', 'dob', 'firstDayOfAWeek', 'xmlFile', 'xmlFileChecksum', 'serverIP4Address', 'localAreaNetwork4Mask', 'maximumNumberOfReturnedSearchRecords', 'maximumNumberOfDisplayedRowsPerPage', 'minimumAgeCriteriaForUsers', 'applicationCounter', 'revisionNumber', 'revisionTime', 'tinNumber', 'systemHash', 'extraFilter', 'extraInformation', 'flags');
	}
	public static function getASearchUI($page, $listOfColumnsToDisplay, $optIndex = 0)    {
		$line = "";
		$mycolumnlist = json_encode($listOfColumnsToDisplay);
		$line .= "&lt;div class=&quot;container __ui_search_container__ py-2&quot;&gt;    &lt;div class=&quot;row&quot;&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;    &lt;form id=&quot;__delta_init_basic__&quot;&gt;        &lt;div class=&quot;input-group mb-3&quot;&gt;            &lt;input name=&quot;__ui_search_input__&quot; id=&quot;__ui_search_input__$optIndex&quot; type=&quot;search&quot; data-min-length=&quot;3&quot;                class=&quot;form-control&quot;required placeholder=&quot;Search&quot; aria-label=&quot;Search&quot; aria-describedby=&quot;basic-addon2&quot; /&gt;            &lt;div class=&quot;input-group-append&quot;&gt;                &lt;button id=&quot;__ui_search_button__$optIndex&quot; data-form-id=&quot;__delta_init_basic__&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot;                    data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot;                    data-column='[&quot;profileName&quot;]' data-page='$page' data-class='Profile'                    class=&quot;btn btn-outline-primary btn-perform-search&quot; type=&quot;button&quot;      data-search-input=&quot;text&quot; data-search-input-id=&quot;__ui_search_input__$optIndex&quot; data-toggle=&quot;tooltip&quot;                    title=&quot;This is a basic search&quot;&gt;Search&lt;/button&gt;            &lt;/div&gt;        &lt;/div&gt;    &lt;/form&gt;&lt;/div&gt;";
		$line .= "&lt;div class=&quot;col-md-6 &quot;&gt;&lt;button type=&quot;button &quot;class=&quot;btn btn-outline-primary btn-block&quot; name=&quot;__ui_advanced_search_button__&quot; id=&quot;__ui_advanced_search_button__$optIndex&quot; data-output-target=&quot;__ui_search_output_target__$optIndex&quot; data-display-column='$mycolumnlist' data-error-target=&quot;__ui_search_error__$optIndex&quot; data-column='[&quot;profileId&quot;,&quot;profileName&quot;,&quot;systemName&quot;,&quot;logo&quot;,&quot;telephoneList&quot;,&quot;emailList&quot;,&quot;otherCommunication&quot;,&quot;website&quot;,&quot;fax&quot;,&quot;city&quot;,&quot;country&quot;,&quot;PHPTimezone&quot;,&quot;dob&quot;,&quot;firstDayOfAWeek&quot;,&quot;xmlFile&quot;,&quot;xmlFileChecksum&quot;,&quot;serverIP4Address&quot;,&quot;localAreaNetwork4Mask&quot;,&quot;maximumNumberOfReturnedSearchRecords&quot;,&quot;maximumNumberOfDisplayedRowsPerPage&quot;,&quot;minimumAgeCriteriaForUsers&quot;,&quot;applicationCounter&quot;,&quot;revisionNumber&quot;,&quot;revisionTime&quot;,&quot;tinNumber&quot;,&quot;systemHash&quot;,&quot;extraFilter&quot;,&quot;extraInformation&quot;,&quot;flags&quot;]' data-min-length=&quot;3&quot; data-page='$page' data-class='Profile' data-search-dialog=&quot;__dialog_search_container_01__&quot; data-toggle=&quot;tooltip&quot; title=&quot;This is a more advanced search technique&quot;&gt;Advanced Search&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;&lt;br/&gt;&lt;div id=&quot;__ui_search_error__$optIndex&quot; class=&quot;p-1 ui-sys-error-message&quot;&gt;&lt;/div&gt;&lt;div style=&quot;overflow-x: scroll;&quot; id=&quot;__ui_search_output_target__$optIndex&quot;&gt;&lt;/div&gt;&lt;/div&gt;";
		$line .= "&lt;script type=&quot;text/javascript&quot;&gt;(function($)    {    $(function()    {        var callbackFunction$optIndex = function(\$button1, data, textStatus, optionArgumentArray1) {            var \$dialog1 = $('#' + \$button1.data('searchDialog'));            \$dialog1 = showAdvancedSearchDialog(\$button1, \$dialog1, data, Constant);            \$dialog1.modal('show');      };        $('#__ui_advanced_search_button__$optIndex').on('click', function(e)   {            var \$button1 = $(this);            var columnList = \$button1.data('column');            var classname = \$button1.data('class');            var payload = { columns: columnList, classname: classname };            fSendAjax(\$button1,                    $('&lt;span/&gt;'),                    '../server/serviceGetAdvancedSearchPayload.php',                    payload,                    null,                    null,                    callbackFunction$optIndex,                    null,                    null,                    &quot;POST&quot;,                    true,                    false,                    &quot;Processing ....&quot;,                    null,                    null);        });    });})(jQuery);&lt;/script&gt;";
		return htmlspecialchars_decode($line);
	}
	public function getMyPropertyValue($pname)  {
		if ($pname == "profileId") return $this->profileId;
		else if ($pname == "profileName") return $this->profileName;
		else if ($pname == "systemName") return $this->systemName;
		else if ($pname == "logo") return $this->logo;
		else if ($pname == "telephoneList") return $this->telephoneList;
		else if ($pname == "emailList") return $this->emailList;
		else if ($pname == "otherCommunication") return $this->otherCommunication;
		else if ($pname == "website") return $this->website;
		else if ($pname == "fax") return $this->fax;
		else if ($pname == "city") return $this->city;
		else if ($pname == "country") return $this->country;
		else if ($pname == "PHPTimezone") return $this->PHPTimezone;
		else if ($pname == "dob") return $this->dob;
		else if ($pname == "firstDayOfAWeek") return $this->firstDayOfAWeek;
		else if ($pname == "xmlFile") return $this->xmlFile;
		else if ($pname == "xmlFileChecksum") return $this->xmlFileChecksum;
		else if ($pname == "serverIP4Address") return $this->serverIP4Address;
		else if ($pname == "localAreaNetwork4Mask") return $this->localAreaNetwork4Mask;
		else if ($pname == "maximumNumberOfReturnedSearchRecords") return $this->maximumNumberOfReturnedSearchRecords;
		else if ($pname == "maximumNumberOfDisplayedRowsPerPage") return $this->maximumNumberOfDisplayedRowsPerPage;
		else if ($pname == "minimumAgeCriteriaForUsers") return $this->minimumAgeCriteriaForUsers;
		else if ($pname == "applicationCounter") return $this->applicationCounter;
		else if ($pname == "revisionNumber") return $this->revisionNumber;
		else if ($pname == "revisionTime") return $this->revisionTime;
		else if ($pname == "tinNumber") return $this->tinNumber;
		else if ($pname == "systemHash") return $this->systemHash;
		else if ($pname == "extraFilter") return $this->extraFilter;
		else if ($pname == "extraInformation") return $this->extraInformation;
		else if ($pname == "flags") return $this->flags;
		return null;
	}
	public static function getValue0Columnname() {
		return "profileName";
	}
	public static function getValueColumnnames()   {
		return array('profileName');
	}
	public function getName() {
		return array($this->profileName);
	}
	public function getName0() {
		$namedValue = $this->profileName;
		return $namedValue;
	}
}
?>